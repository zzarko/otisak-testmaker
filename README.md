Testmaker 3.0
=============

*Testmaker* je program za generisanje *XML* fajlova za testove za
program *Otisak*, kao i za generisanje *OpenDocument Text* fajlova sa
testovima koji se mogu odštampati. Kao svoj ulaz koristi testove
zadate u tekstualnom obliku. Izrađen je korišćenjem flex‑a i bison‑a.
Sastoji se od parsera tekstualnog opisa pitanja, parsera tekstualnog
opisa testa i generatora *XML* fajlova (sadržaj *XML* fajlova je
statički definisan u defs.h fajlu). Autor programa je Žarko Živanov.

Program radi sa standardnim *ASCII* tekstom i sa našim kvačicama u
*UTF-8* (i opciono u *Latin2*, odnosno *ISO 8859‑2*) kodnom rasporedu.
Za ispravan rad se mora pokrenuti iz direktorijuma u kome se nalazi, i
mora se poštovati unapred zadata struktura direktorijuma:

-   u osnovnom direktorijumu se nalaze:
    -   izvršni fajl testmaker
    -   skript za generisanje zip/odt fajlova zipmaker
    -   skript za generisanje svih testova i zip fajlova generateall
    -   poddirektorijumi questions, tests, images, material, source,
        docs, odt, transcode i output
-   u poddirektorijum questions se stavljaju tekstualni fajlovi sa
    ekstenzijom txt u kojima se nalaze pitanja; radi lakšeg snalaženja,
    naziv fajla bi trebao da odgovara oblasti za koju su pitanja
    napisana
-   u poddirektorijum tests se stavljaju tekstualni fajlovi sa
    ekstenzijom txt u kojima se nalaze opisi testova
-   u poddirektorijum images se stavljaju slike koje će se koristiti u
    pitanjima (ukoliko se u definiciji pitanja nalazi ime slike, ista će
    biti iskopirana u odgovarajući test prilikom njegovog generisanja)
-   u poddirektorijimu materials se nalaze materijali za testove; svaki
    poddirektorijum u njemu predstavlja posebnu varijantu materijala;
    poddirektorijum default sadrži podrazumevani materijal (kopira se
    ako se ne specificira poseban materijal); poddirektorijum empty
    sadrži HTML kod prazne dokumentacije (sadrži funkcije neophodne za
    kontrolisano izvršavanje *firefox-a* i treba ga koristiti kao osnovu
    za druge materijale)
-   u poddirektorijimu source se nalazi izvorni kod programa
-   u poddirektorijimu odt se nalaze šabloni za generisanje odt fajlova
-   u poddirektorijimu docs se nalazi uputstvo za program u ods i pdf
    formatu, kao i fajl sa revizijom izmena history.txt
-   u poddirektorijimu transcode se nalaze fajlovi potrebni za
    konverziju naših slova u odgovarajuće kodove koje poznaje *Otisak*
-   u poddirektorijumu output se kreira novi poddirektorijum istog
    naziva kao i test, unutar kojeg se formiraju poddirektorijumi za
    pojedine grupe (npr. AR\_T1\_G1-A)

Za imena svih fajlova koji se prave važi da moraju biti sačinjena od
malih i velikuh slova, cifara i znakova minus, tačka i donja crta
(razmaci nisu dozvoljeni). Glavni program se pokreće sa jednim
parametrom koji predstavlja naziv tekstualnog fajla (bez ekstenzije)
od kojeg treba generisati test, i opcionim parametrom za tip
konverzije:

    /testmaker [-e <tip_kodiranja>] <naziv_testa>

Tip kodiranja može biti utf8 (podrazumevano) ili latin2 i odnosi se se
na ulazne fajlove sa opisima pitanja i testova.

Kada su svi testovi izgenerisani, odgovarajući zip fajlovi se mogu
napraviti pozivanjem skripta zipmaker:

    ./zipmaker [naziv_testa]

zip fajlovi će biti izgenerisani u poddirektorijumu sa nazivom testa u
output direktorijumu (prethodno će svi zip fajlovi iz tog
poddirektorijuma biti obrisani). Ukoliko se navede naziv testa, biće
generisani zip fajlovi za taj test, u suprotnom će se generisati zip
fajlovi za sve testove iz output direktorijuma. Zajedno sa zip
fajlovima, u istom direktorijumu će se generisati i odgovarajući odt
fajlovi. Odgovori u odt fajlovima se ređaju na slučajan način, ali je
redosled pitanja fiksan i definisan je redosledom pitanja u testu.

**NAPOMENE** za generisanje odt fajlova:

-   generisanje odt fajlova je veoma osetljivo
    na greške u fajlovima sa pitanjima, što se prvenstveno odnosi na
    nezatvorene tagove (bold, italic, code), odnosno svi tagovi moraju
    biti zatvoreni; ako *testmaker* detektuje nepravilne tagove, fajlovi
    za test neće biti izgenerisani.
-   slike se, zasad, ne ubacuju u odt fajlove
-   korektno su podržana /t, /c i /u pitanja
-   očekuje se da svako pitanje ima bar jedan tačan odgovor; ako to nije
    slučaj, nikakva dodatna opcija neće biti ispisana (kao što se radi
    za *Otisak*), a na poslednjoj stranici sa tačnim odgovorima, odgovor
    za takvo pitanje će biti prazan

Skript generateall će za svaki test iz tests poddirektorijima prvo
pozvati testmaker, a nakon toga i zipmaker (ukoliko nije bilo grešaka
prilikom generisanja testa).

### Vrste pitanja

Postoje 3 vrste pitanja:

-   pitanja sa izborom 1 od n (*radio button*); deli se na dve podvrste:
    pitanja kod kojih treba označiti tačan odgovor i pitanja kod kojih
    treba označiti netačan odgovor
-   pitanja sa izborom m od n, gde je m<n (*checkbox*), i
-   pitanja sa unošenjem teksta (*freetext*).

Bodovanje prve vrste (1 od n) je realizovano ovako: ako je obeležen
tačan odgovor, dobijaju se pozitivni poeni, dok se za bilo koji
netačan odgovor dobijaju negativni poeni.

Bodovanje druge vrste (m od n) je realizovano ovako: ako je obeleženo
1 ili više tačnih odgovora i ako nije obeležen nijedan netačan
odgovor, dobijaju se pozitivni poeni u zavisnosti od broja tačnih
odgovora (pozitivni poeni dodeljeni pitanju se ravnomerno dele na sve
tačne odgovore). Ako je označen bar 1 netačan odgovor, dobijaju se
negativni poeni (nezavisno od broja zaokruženih netačnih odgovora).
Ukoliko se ne koristi skaliranje (videti poglavlje o opisu testa),
broj poena za ovakvo pitanje treba da bude deljiv sa brojem tačnih
odgovora.

Bodovanje treće vrste (unos) je realizovano ovako: ako je unet tačan
odgovor, dobijaju se pozitivni poeni, dok se za netačan odgovor
dobijaju negativni poeni.

### Format fajla sa pitanjima

Fajl sa pitanjima se sastoji od tekstualnog opisa proizvoljnog broja
pitanja i mora imati ekstenziju txt. Ukoliko neka linija
počinje znacima `//`, smatraće se komentarom i neće se analizirati. Od
verizje 2.5.2, iza oznake vrste pitanja se može dodati oznaka e, što
označava da će se na kraju tog pitanja nalaziti ugrađeno *edit* polje
kada se pitanje prikaže u *Otisku*. Svrha ovog polja je da se
studentima olakša odgovaranje na pitanja za koja bi u *Otisku* morali
često da se prebacuju na tab sa beleškama.

#### Pitanja kod kojih treba označiti tačan odgovor

    //identifikacija može biti ili pitanje_id ili grupa_id*pitanje_id
    *shell*p1
    /t ili /te
    Listanje direktorijuma u <i>bash shell</i>-u se ostvaruje komadom:
    =slika.png
    -cat
    -pwd
    -cd
    -less
    +ls
    .

U prvoj liniji se nalazi oznaka `*` iza koje (bez razmaka) sledi
identifikacija grupe pitanja (shell) i identifikacija pitanja unutar
grupe (p1). Ove dve identifikacije su međusobno razdvojene oznakom `*`.
Identifikacija može biti bilo koja kombinacija malih slova, velikih
slova, cifara, donje crte, minusa i tačke. Ukoliko se u fajlu nađu dva
pitanja sa istom identifikacijom, samo prvo će biti prepoznato kod
generisanja pitanja. Oznaka grupe se može izostaviti, čime se zadržava
kompatibilost sa označavanjem pitanja za *testmaker* verzije 1.x.
Ukoliko se u jednom fajlu nalaze i jedna i druga vrsta oznaka pitanja,
tada bi id-ovi pitanja koja nemaju grupu i grupe morali biti
jednistveni.

U drugoj liniji se nalazi oznaka `/t` koja
označava vrstu pitanja (pitanje kod kojeg treba označiti jedan tačan
odgovor). Ukoliko se iza ove oznake nalazi i slovo e
(`/te`), tada će na kraju pitanja biti dodato *edit* polje.

U nastavku se nalaze jedna ili više linija sa tekstom pitanja. Tekstom
pitanja se smatra svaka linija koja **ne počinje** jednim od sledećih
znakova: `/` (slash), `=` (jednako), `+` (plus), `-` (minus), `.` (tačka).

Iza svake linije, ukoliko iza nje ima još linija, se automatski
ubacuje *HTML* tag za prelazak u novi red (`br`).

Ukoliko linija počinje razmakom ili tabulatorom, smatraće se da je to
programski kod i takve linije će biti ispisane neproporcionalnim
fontom (uz upotrebu div class=”Code” *HTML* taga), s tim da će
indentacija biti očuvana (svaki razmak se konvertuje u
*non‑breaking space*, a vodeće tabulacije se konvertuju u 4
*non‑breaking space*‑a). Preporuka je da se koriste razmaci, time se
osigurava da će izgled u *Otisku* biti isti kao u editoru.

U tekstu pitanja se mogu nalaziti i oznake za početak i kraj podebljanog teksta
(`\B` i `\b`, odnosno `<b>` i `</b>`), početak i kraj iskošenog teksta (`\I` i `\i`, odnosno
`<i>` i `</i>`) i početak i kraj teksta sa neproporcionalnim fontom (`\C` i `\c`,
odnosno `<c>` i `</c>`) koje se konvertuju u odgovarajući HTML kod. Svi ostali znaci koji mogu biti
problematični za HTML/Otisak kod (`<,>,&,[,],”,|`) se automatski konvertuju u
odgovarajuće HTML kodove. Ukoliko je potrebno da se u tekstu nađe baš string
`<i>`, to se može napisati kao `\<i\>`.

Nakon teksta pitanja se opciono može nalaziti naziv slike koja ide uz
pitanje i ona se navodi u posebnoj liniji, neposredno iza oznake =.
Navedena slika se automatski kopira iz images direktorijuma prilikom
generisanja testa. Ukoliko slika nije potrebna, ova linija se
izostavlja.

Iza naziva slike, odnosno teksta pitanja, slede stavke odgovora. Jedan
odgovor se **mora nalaziti u jednoj liniji**. Stavke koje nose negativne
poene počinju znakom -, a (najviše) jedna stavka može nositi pozitivne
poene i ona započinje znakom +. Konverzija specijalnih kodova se radi i
za stavke odgovora. Ukoliko odgovor (iza znaka +, odnosno -) počinje
razmakom ili tabulatorom, smatraće se da je to programski kod i takav
odgovor će biti ispisan neproporcionalnim fontom (uz upotrebu
div class=”Code” *HTML* taga). Pored stavki koje su ovde navedene,
ukoliko je to dozvoljeno (što je podrazumevano podešavanje), u
izgenerisano pitanje se automatski ubacuje i stavka u stilu “svi
odgovori iz prethodne liste su pogrešni”. Kod generisanja **odt** fajla,
prethodna stavka može takođe biti ubačena (ovo nije podrazumevano
podešavanje). Na kraj svakog *radio‑button* pitanja (tipovi `/t` i `/n`) će
se dodati i stavka “bez odgovora” koja se dodaje iza poslednjeg bloka u
pitanju i koja je ujedno podrazumevana kada se studentu prvi put prikaže
pitanje. Raspored pozitivnih i negativnih odgovora je proizvoljan.
Ukoliko se detektuje da je tekst u dve stavke identičan, prijaviće se
greška.

Ukoliko je potrebno ubaciti još jedan blok u pitanje, nakon stavki
treba na početku posebne linije staviti oznaku /, iza koje ponovo idu
tekst, opciona slika i stavke (ovo odgovara novom *BLOCK*‑u u *XML*
fajlu).

Kraj pitanja se označava oznakom . (tačka).

#### Pitanja kod kojih treba označiti netačan odgovor

    *algebra*1
    /n ili /ne
    Rezultat sabiranja <c>5+3</c> je isti kao rezultat:
    -oduzimanja 13-5
    -deljenja 16/2
    -mnozenja 2*4
    -stepenovanja 2^3
    +sabiranja 2+7
    .

Sintaksa je ista kao i kod prethodne vrste pitanja, razlika je samo u
tome što je ovde oznaka pitanja `/n`. Ukoliko se iza ove oznake nalazi i
slovo e (`/ne`), tada će na kraju pitanja biti dodato
*edit* polje. Sve napomene date za prethodnu vrstu pitanja važe i
ovde.

#### Pitanja sa više tačnih odgovora

    *algebra*2
    /c ili /ce
    Rezultat sabiranja <c>5+3</c> je isti kao rezultat:
    +oduzimanja 13-5
    +deljenja 16/2
    -mnozenja 2*5
    +stepenovanja 2^3
    -sabiranja 2+7
    .

Sintaksa je slična kao i kod prethodne vrste pitanja, razlika je u
tome što je ovde oznaka pitanja `/c` i što može biti više tačnih
odgovora. Ukoliko se iza ove oznake nalazi i slovo e (`/ce`), tada će na
kraju pitanja biti dodato *edit* polje. Mora postojati
bar jedan tačan i bar jedan netačan odgovor. Sve napomene date za
prethodne vrste pitanja važe i ovde.

#### Pitanja sa unošenjem teksta

    *p1
    /u ili /ue
    Ako imaš 127 jabuka i nekom daš sve osim 39, koliko ti ostaje?
    (odgovor upisati kao <b>dekadni broj</b> bez vodećih nula)
    \Preostalo jabuka\39
    .

Ova pitanja se označavaju oznakom `/u`. Ukoliko se iza ove oznake nalazi
i slovo e (`/ue`), tada će na kraju pitanja biti dodato
*edit* polje. Tekst pitanja se unosi isto kao i kod prethodne dve
vrste pitanja. Takođe, nakon teksta pitanja se opciono može nalaziti
oznaka za sliku.

Iza teksta, odnosno naziva slike, se nalazi oznaka `\` iza koje se
navodi naziv polja za unos. Iza naziva polja za unos se ponovo navodi
znak `\` i nakon njega vrednost tačnog odgovora. Ukoliko se u pitanju
nalazi više blokova, polje za unos se može nalaziti u samo jednom od
njih. Svi razmaci i tabulacija sa početka i kraja kod oba polja u ovoj
liniji se odstranjuju.

#### Programsko generisanje teksta pitanja

    *zbir
    /c
    |luainit
    Broj {{n=rnd(40)+10; return n*2}} se može predstaviti kao zbir:
    +<c>{{return n-3}}+{{return n+3}}</c>
    +<c>{{return n-7}}+{{return n+7}}</c>
    -<c>{{return n-4}}+{{return n+3}}</c>
    -<c>{{return n-7}}+{{return n+6}}</c>
    |{{return n}}
    .

Tekst koji se odnosi na pitanja može sadržati proizvoljan broj
programski generisanih stringova. Programski jezik koji se koristi za
ovo je *LUA*, verzija 5.3.4. U ovom uputstvu akcenat neće biti na *LUA*
jeziku, nego na njegovoj integraciji u *testmaker*. Za informacije o
samom *LUA* jeziku i njegovoj uoptrebi se treba informisati na
http://www.lua.org, ili pomoću neke druge literature.

Na bilo kom mestu u tekstu pitanja ili odgovora se može ubaciti *LUA*
kod unutar duplih vitičastih zagrada:

    {{ <lua_kod> }}

Pošto se fajlovi sa testovima analiziraju liniju po liniju, sav *LUA*
kod unutar jednih duplih vitičastih zagrada se mora nalaziti u istoj
liniji, odnosno ne sme se prelamati na sledeću liniju.

Poslednji iskaz u svakom *LUA* kodu mora biti return koji kao argument
ima **string** ili **broj**. String koji se na ovaj način vrati će
prilikom generisanja teksta pitanja biti ubačen umesto *LUA* koda. Na
primer, ako linija u tekstu pitanja ima sledeći sadržaj:

Jedna od divljih životinja je i `{{return “zebra”}}`.

tada će se prilikom generisanja teksta pitanja *LUA* kod zameniti
stringom kojeg on generiše:

Jedna od divljih životinja je i zebra.

Naravno, kod koji uvek proizvodi isti tekst nije preterano koristan, te
je korišnjenje math.random funkcije, nizova ili bilo kojih drugih
konstrukcija *LUA* jezika neophodno kako bi se mogla generisati slična
pitanja sa različitim rezultatima. Za par jednostavnijih primera se može
pogledati fajl uvod-lua.txt.

Za slučaj da je potreban komplikovaniji kod, on se može smestiti u
funkcije u zasebnim fajlovima. Ukoliko pitanju treba fukncija koja se
nalazi u zasebnom fajlu, takav fajl treba postaviti u direktorijum sa
pitanjima, sa ekstenzijom .lua, na primer:

    testmaker/questions/luainit.lua

Da bi funkcije i inicijalizacije koje se nalaze u skriptu bile pokrenute
pre nego se krene na generisanje teksta pitanja, nakon oznake vrste
pitanja treba dodati liniju koja definiše naziv skripta i koja započinje
znakom `|` (pipe):

    |<naziv_lua_skripta>

Naziv skripta se može zadati sa ili bez .lua ekstenzije. Isti skript se
može koristiti i u više pitanja, ali će se pozvati samo jednom, prilikom
prvog korišćenja. Odnosno, inicijalizacije u skript fajlu će se odraditi
samo jednom za ceo test, bez obzira na to u koliko se pitanja taj skript
fajl koristio.

Za sva pitanja u okviru jednog testa se inicijalizuje jedna instanca
*LUA* interpretera i svi proračuni se izvršavaju unutar nje. Sav *LUA*
kod unutar jednog pitanja se izvršava u redosledu u kom je naveden u
tekstu pitanja. To znači da se u okviru jednog pitanja u jednom *LUA*
kodu može postaviti neka promenljiva, pa onda kasnije koristiti u drugom
*LUA* kodu. Iako se vrednosti promenljivih čuvaju i između različitih
pitanja, nije preporučljivo da se to i koristi, pošto redosled pitanja u
testu može biti proizvoljan.

Kako bi se obezbedila kontrola da li se desilo da se izgenerišu dva ista
pitanja, sva pitanja koja u sebi imaju *LUA* kod mogu u poslednjoj
liniji imati oznaku za generisanje identifikatora pitanja koja započinje
znakom | (pipe). Identifikator pitanja je jednostavno neki string ili
broj koji na neki način zavisi od vrednosti izgenerisanih u pitanju:

    |{{return <identifikator>}}

Na primer, ako je pitanje vezano za sabiranje dva broja, identifikator
bi mogao biti zbir (tada bi se pojavilo upozorenje ako je dva puta
izgenerisano pitanje sa istim zbirom), ili bi mogao biti string koji se
sastoji od oba sabirka (tada bi se pojavilo upozorenje ako je dva puta
izgenerisanjo pitanje sa istim prvim i drugim sabircima). Odnosno, šta
bi bio zgodan identifikator, zavisi od pitanja. I pored ovoga, može se
desiti da se izgenerišu ista pitanja (zbog načina rada random funkcije i
skaliranja na zadati opseg), što će biti propraćeno sa “same LUA ID” na
kraju upozorenja, pa kada se to desi, generisanje testa treba ponoviti
još jednom ili treba doraditi kod za generisanje stringova u pitanju da
ovako nešto bude manje verovatno.

Ako se ovakav identifikator ne postavi u pitanje sa *LUA* kodom, tada će
*testmaker* izbaciti upozorenje svaki put kada se ovo pitanje ponovi u
jednom testu i onda je osoba koja sastavlja test dužna da proveri da li
su pitanja stvarno identična.

### Format fajla sa opisom testa

Fajl sa testovima se sastoji od tekstualnog opisa proizvoljnog broja
testova i mora imati ekstenziju txt. Ukoliko neka linija počinje
znacima //, smatraće se komentarom i neće se analizirati.

    / SKAL=1000 NEGOD=3 POZ=2000 NEG=-1000 ISPT=0 EDIT=0 SRAND=1 SVIODG=1 TESTP=0 DETALJI=1
    =podrazumevani_materijal

    AR_T1_G1 1
    "Uvodni test" "Primer formata pitanja na stvarnom testu."
    =mobilni
    uvod 1 5 -1 1
    uvod 2 5 -1 1
    uvod 3 5 -1 0
    uvod 4 5 -1 1
    uvod 5 5 -1 1
    uvod 6 5 -1 0
    .

U prvoj liniji se nalaze opcioni parametri (mogu da se
izostave i tada su njihove podrazumevane vrednosti SKAL=1, NEGOD=1,
POZ=1, NEG=-1, ISPT=0, EDIT=0, SRAND=0, SVIODG=1, TESTP=0, DETALJI=1;
ako se navode, ne moraju se navesti svi i mogu se navesti u proizvoljnom
redosledu). Značenje im je sledeće:

1.  SKAL označava vrednost skaliranja poena. Ako se navede, npr.
    1000, to znači da će vrednosti poena u definiciji testa
    predstavljati mili-poene (ako uz neko pitanje stoji npr. 2000,
    onda će stvaran broj poena biti 2). Ovo efektivno omogućava da
    broj poena na pojedine odgovore bude razlomljen (pogledati
    primer testa koji se nalazi u arhivi).
2.  NEGOD određuje od koliko netačno odgovorenih pitanja će se
    računati negativni poeni. Ako je ovaj parametar npr. 3 i ako je
    neko na testu pogrešno odgovori na manje od 3 pitanja, neće mu
    se računati negativni poeni, nego će za ta pitanja dobiti 0
    poena.
3.  POZ predstavlja podrazumevane pozitivne poene
4.  NEG predstavlja podrazumevane negativne poene
5.  ISPT određuje da li će se na kraju svakog checkbox pitanja na
    testu naći tekst “(broj tačnih odgovora u pitanju: X)”.
6.  EDIT određuje da li će se ispod svakog pitanja naći edit
    polje (1) ili će se edit polje nalaziti samo kod onih pitanja
    kod kojih je to definisano (0).
7.  SRAND određuje vrednost koja će se proslediti
    srand() funkciji na početku rada programa. Ukoliko
    se navede vrednost 0, funkcija će se pozvati sa srand(time(0)).
    Zadavanje konkretne vrednosti za SRAND omogućava generisanje
    uvek istog redosleda pitanja ukoliko se kod zadavanja pitanja
    navede samo id grupe, što je verovatno poželjno ponašanje u toku
    pravljenja testa.
8.  SVIODG određuje da li će se na kraju *radio‑button* pitanja naći
    i stavka u stilu “svi odgovori iz prethodne liste su pogrešni”.
    Vrednost 0 ovakvu stavku izostavlja (čime sva pitanja koja imaju
    samo pozitivne ili samo negativne odgovore postaju pogrešna i za
    njih će se prijaviti greška), vrednost 1 je ubacuje, a vrednost
    2 je ubacuje i u **odt** fajlove.
9.  TESTP služi za proveravanje da li svaki test ima zadati broj
    poena. Vrednost 0 znači da ove provere neće biti, dok
    **neskalirana** vrednost veća od nule znači da ukoliko neki test
    nema taj zadati broj poena, ispisaće se poruka o grešci.
10. DETALJI određuje da li će se studentima, kada istekne vreme
    testa, prikazati detaljni rezultati sa tačnim odgovorima za
    svako pitanje. Vrednost 0 znači da se detalji neće prikazati, a
    vrednost 1 da hoće.

U drugoj liniji se nalazi (takođe opcioni) podrazumevani materijal za
sve testove (ako se u pojedinom testu ne navede stavka za materijal,
koristiće se ovaj). Ukoliko se ova linija ne navede, podrazumevani
materijal će biti prazan (empty) i prilikom testa se neće ni prikazivati
tab sa materijalima.

U trećoj liniji se nalaze oznaka testa i grupa. Oznaka testa se sastoji
od malih i velikih slova, cifara, znaka donja crta, tačke i minusa, s
tim da oznaka ne može početi tačkom ili minusom. Grupa može biti 0 (test
koji ide za obe grupe), 1, a ili A (prva grupa) ili 2, b ili B (druga
grupa). Ukoliko je grupa 1 ili a, naziv grupe će biti konvertovan u A,
odnosno ukoliko je grupa 2 ili b, naziv grupe će biti konvertovan u B.
Korišćenjem ova dva polja se pravi naziv poddirektorijuma u kome će se
nalaziti izgenerisani fajlovi (u gornjem primeru, naziv direktorijuma bi
bio AR\_T1\_G1-A), a to je ujedno i vrednost polja *KURS* u *XML* fajlu.

U četvrtoj liniji se nalaze naziv testa (*NAZIV* u *XML* fajlu) i kratak
opis testa (*UVOD* u *XML* fajlu). Oba podatka se moraju nalaziti unutar
dvostrukih znakova navoda.

U petoj liniji se nalazi naziv opcionog materijala koji će se koristiti
na testu. Ukoliko je naveden, sadržaj poddirektorijuma sa tim nazivom iz
direktorijuma materials će biti iskopiran u materijal za test. Ukoliko
materijal nije naveden, kao materijal će se koristiti podrazumevani
materijal. Specijalan materijal sa nazivom empty se koristi ako nije
naveden ni ovaj ni podrazumevani materijal (ovaj naziv se može koristiti
za izbacivanje materijala za pojedinu grupu, ako je zadat podrazumevani
materijal). empty materijal podrazumeva da se tab sa materijalima neće
ni prikazivati prilikom testiranja. Materijal mora biti u *HTML* obliku,
i mora sadržavati elemente koji postoje u empty materijalu (*JavaScript*
kod kao u index.html fajlu mora biti na svakoj *HTML* stranici i mora
postojati include/functions.js fajl).

U narednim linijama se nalazi opis pitanja od kojih se test sastoji.
Ovih linija može biti proizvoljno mnogo. Svaka linija se sastoji iz 5
polja:

    naziv\_oblasti id poeni+ poeni- izbacivanje

Polje `naziv_oblasti` se odnosi na naziv tekstualnog fajla (bez
ekstenzije) iz koga treba uzeti pitanje.

Polje `id` se odnosi na identifikaciju pitanja (oznaka na početku, iza
zvezdice), grupe, ili i pitanja i grupe. Ako se navede samo id pitanja
(npr. p1), tada će se u odgovarajućem fajlu tražiti pitanje sa zadatim
id-om (pitanja koja u oznaci nemaju oznaku grupe). Ako se navedu i
oznaka grupe i oznaka pitanja razdvojeni znakom `*` (npr. `algebra*1`),
tada će se u odgovarajućem fajlu tražiti pitanje sa zadatom grupom i
id-om (pitanja koja u oznaci imaju i oznaku grupe). Ako se navede samo
oznaka grupe (npr. algebra), tada će se u odgovarajućem fajlu po
slučajnom izboru odabrati jedno neiskorišteno pitanje iz zadate grupe.

Polje `poeni+` se odnosi na poene za ispravan odgovor, dok se polje `poeni-`
se odnosi na poene za neispravan odgovor (ako se ovde zada negativan
broj, u tekst pitanja se upisuje i napomena o tome da se mogu dobiti
negativni poeni). Ukoliko su oba ova polja jednaka nuli, tada se za
pitanje uzima podrazumevani broj pozitivnih, odnosno negativnih poena
(kako je definisano u parametrima testa).

Polje `izbacivanje` se odnosi samo na pitanja se izborom jednog odgovora
(kod ostalih pitanja se ignoriše). Ovime se označava koja od stavki će
se izbaciti iz pitanja. Ukoliko se ovde navede 0, izbaciće se stavka
koja nosi pozitivne poene, a ako se navede broj veći od 0, izbaciće se
jedna od stavki koje nose negativne poene. Izbacivanje stavke koja nosi
pozitivne poene povlači za sobom da stavka “svi odgovori iz prethodne
liste su tačni/pogrešni” postane stavka sa pozitivnim brojem poena.
Ukoliko se navede negativan broj ili broj koji je veći od broja stavki
sa negativnim brojem poena, u izgenerisanom pitanju će se naći sve
stavke.

Kraj testa se označava oznakom `.` (tačka).

#### Zadavanje opcija testa za testmaker 2.5.0 i starije verzije

Verzije *testmaker*‑a pre 2.5.2 su koristile drugačiji format zadavanja
opcija. Ovaj format se može i dalje koristiti, ali se preporučuje da se
pređe na novi. Zadavanje izgleda ovako:

    /1 1 1 -1 0

Ako se parametri izostave, tada su njihove podrazumevane vrednosti 1, 1,
1, -1 i 0 respektivno, što odgovara testovima za *testmaker* verzije
1.x. Ako se parametri navode, moraju se navesti svi. Ima ih 5 i značenje
im je sledeće:

1.  Prvi parametar označava vrednost skaliranja poena.
2.  Drugi parametar određuje od koliko netačno odgovorenih pitanja
    će se računati negativni poeni.
3.  Treći parametar predstavlja podrazumevane pozitivne poene
4.  Četvrti parametar predstavlja podrazumevane negativne poene
5.  Peti parametar određuje da li će se na kraju svakog checkbox
    pitanja na testu naći tekst “(broj tačnih odgovora u pitanju: X)”.

Parametri za ubacivanje edit polja i inicijalne vrednosti `srand()`
funkcije se ovde ne mogu zadati i njihove vrednosti će biti 0.
