/*****************************************************************************
/*****************************************************************************/

  var up, down;
  var min1, sec1;
  var cmin1, csec1, cmin2, csec2;


  function popup(url, width, height)
  {
      day = new Date();
      id = day.getTime();
      eval("page" + id + " = window.open(url, '" + id + "', 'toolbar = 0, scrollbars = 1, location = 0, statusbar = 0, menubar = 0, resizable = 1, width = " + width + ", height = " + height + "');");
  }

  function goto_url(object)
  {
      window.location.href = object.options[object.selectedIndex].value;
  }

  function minutes(data)
  {
      for (var i = 0; i <data.length; i++) if (data.substring(i, i + 1) == ":") break;
      return (data.substring(0, i));
  }

  function seconds(data)
  {
      for (var i = 0; i <data.length; i++) if(data.substring(i, i + 1) == ":") break;
      return (data.substring(i + 1, data.length));
  }

  function Display(min, sec)
  {
      var disp;
      if (min <= 9) disp = "0";
      else disp = " ";
      disp += min + ":";
      if (sec <= 9) disp += "0" + sec;
      else disp += sec;
      return (disp);
  }

  function Down()
  {
      cmin2 = 1 * minutes(document.countdown.start_time.value);
      csec2 = 0 + seconds(document.countdown.start_time.value);
      DownRepeat();
  }

  function DownRepeat()
  {
      if (--csec2 == -1) {
          csec2 = 59;
          cmin2--;
      }

      document.countdown.time.value = Display(cmin2, csec2);
      if ((cmin2 == 0) && (csec2 == 0)) alert("Time's up!");
      else down = setTimeout("DownRepeat()", 1000);
  }
  
// Postovanje kada se pritisne link na neko pitanje... Kurton varijanta u kojoj se stisne vec postojece dugme, da ne bi imali tajnih dugmica  
  
  function PostAndGo(question)
  {
  //document.test.QuestNo.value=question;
  //document.test.next.value="";
  //document.test.prev.value="";
  //if (document.test.next) document.test.next.click(); else document.test.prev.click();
  //document.test.QuestNo.click();
  //alert("stigao sam ovde");
 document.test.QuestNo.value=question;
 document.test.submit();
  return;
  }

  function NullQuestNo() {
    document.test.QuestNo = NULL;
  }

  function confirmation(text) {
    var answer = confirm(text)
     if (answer){
     	
     	  PostAndGo('end');
     }
     else{
     	return false;
     }
  }
  
 function LeavePage(text)
 {
 	//Ako se strana napusta bez pritisnutog ijednog tastera, onda znaci da je vreme 
 	//isteklo i zadnja forma treba da se postuje
 	//ovo je i u slucaju da genije proba da ode sa testa na neku bezveze stranu
 	//DORADITI I ISPRAVITI
 	//if (document.test.QuestNo.value=='') 	PostAndGo('end');
 	}

function end_test()
{
	if (location.href.substring(location.href.lastIndexOf("/") + 1)=="vidi_rezultate.php") location.reload()
	else PostAndGo('end');
} 	
// ============= DisableKeys.js =============//

// Zabranjeni su SVE kombinacije sa ALT ili CTRL tasterima i funkcijski tasteri

var badKeys = new Object();
badKeys.single = new Object();
badKeys.single['112'] = 'F1'
badKeys.single['113'] = 'F2'
badKeys.single['114'] = 'F3'
badKeys.single['115'] = 'F4'
badKeys.single['116'] = 'F5'
badKeys.single['117'] = 'F6'
badKeys.single['118'] = 'F7'
badKeys.single['119'] = 'F8'
badKeys.single['120'] = 'F9'
badKeys.single['121'] = 'F10'
badKeys.single['122'] = 'F11'
badKeys.single['123'] = 'F12'

//TODO: napraviti i funkciju za Eksplorer

function checkKeyCode(type, charCode, keyCode) {
if (type == "alt" || type == "ctrl" ) {
return true;
} else {
if (charCode=="0" && badKeys.single[keyCode]) {window.status = keyCode; return true; }
else 
 return false;
 }
}


var ie=document.all;
var w3c=document.getElementById&&!document.all;

function keyEventHandler(evt) {
this.target = evt.target || evt.srcElement;
this.keyCode = evt.keyCode || evt.which;
this.charCode = evt.charCode;
var targtype = this.target.type;
if (w3c) {
if (document.layers) {
this.altKey = ((evt.modifiers & Event.ALT_MASK) > 0);
this.ctrlKey = ((evt.modifiers & Event.CONTROL_MASK) > 0);
this.shiftKey = ((evt.modifiers & Event.SHIFT_MASK) > 0);
} else {
this.altKey = evt.altKey;
this.ctrlKey = evt.ctrlKey;
this.shiftKey = evt.shiftKey;
}
// Internet Explorer
} else {
this.altKey = evt.altKey;
this.ctrlKey = evt.ctrlKey;
this.shiftKey = evt.shiftKey;
}
// Find out if we need to disable this key combination
var badKeyType = "single";
if (this.ctrlKey) {
badKeyType = "ctrl";
} else if (this.altKey) {
badKeyType = "alt";
} else if (this.shiftKey){
badKeyType = "shift";
}

if (checkKeyCode(badKeyType, this.charCode, this.keyCode)) {
return cancelKey(evt, this.keyCode, this.target);
}

}

function cancelKey(evt, keyCode, target) {

// Ovaj deo je za budzenje ako treba zabraniti i <- i return van forme!
//if (keyCode==8 || keyCode==13) {
// Don't want to disable Backspace or Enter in text fields
//if (target.type == "text" || target.type == "textarea") {
//return true;
//}
//}

if (evt.preventDefault) {
evt.preventDefault();
evt.stopPropagation();
} else {
evt.keyCode = 0;
evt.returnValue = false;
}
return false;
}
// ============= DisableKeys.js =============//

//=====================Paljenje i gasenje prozora za vezbu i materijal
var newWindow = new Array();
var i=0;
function makeNewWindow(URL) {
newWindow[i] = window.open(URL);
newWindow[i].opener.focus();
i++;
}


function closeWindow() {
//newWindow[0].close();
//newWindow[1].open('','_self','');
//newWindow[1].close();
//window.opener.close();
self.focus();
   window.open('','_parent','');
      window.close();
//      window.opener.close();
//      newWindow[0].close();
      }