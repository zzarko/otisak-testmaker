/*****************************************************************************
/*****************************************************************************/

 
// ============= DisableKeys.js =============//

// Zabranjeni su SVE kombinacije sa ALT ili CTRL tasterima i funkcijski tasteri

var badKeys = new Object();
badKeys.single = new Object();
badKeys.single['112'] = 'F1'
badKeys.single['113'] = 'F2'
badKeys.single['114'] = 'F3'
badKeys.single['115'] = 'F4'
badKeys.single['116'] = 'F5'
badKeys.single['117'] = 'F6'
badKeys.single['118'] = 'F7'
badKeys.single['119'] = 'F8'
badKeys.single['120'] = 'F9'
badKeys.single['121'] = 'F10'
badKeys.single['122'] = 'F11'
badKeys.single['123'] = 'F12'

//TODO: napraviti i funkciju za Eksplorer

function checkKeyCode(type, charCode, keyCode) {
if (type == "alt" || type == "ctrl" ) {
return true;
} else {
if (charCode=="0" && badKeys.single[keyCode]) {window.status = keyCode; return true; }
else 
 return false;
 }
}


var ie=document.all;
var w3c=document.getElementById&&!document.all;

function keyEventHandler(evt) {
this.target = evt.target || evt.srcElement;
this.keyCode = evt.keyCode || evt.which;
this.charCode = evt.charCode;
var targtype = this.target.type;
if (w3c) {
if (document.layers) {
this.altKey = ((evt.modifiers & Event.ALT_MASK) > 0);
this.ctrlKey = ((evt.modifiers & Event.CONTROL_MASK) > 0);
this.shiftKey = ((evt.modifiers & Event.SHIFT_MASK) > 0);
} else {
this.altKey = evt.altKey;
this.ctrlKey = evt.ctrlKey;
this.shiftKey = evt.shiftKey;
}
// Internet Explorer
} else {
this.altKey = evt.altKey;
this.ctrlKey = evt.ctrlKey;
this.shiftKey = evt.shiftKey;
}
// Find out if we need to disable this key combination
var badKeyType = "single";
if (this.ctrlKey) {
badKeyType = "ctrl";
} else if (this.altKey) {
badKeyType = "alt";
} else if (this.shiftKey){
badKeyType = "shift";
}

if (checkKeyCode(badKeyType, this.charCode, this.keyCode)) {
return cancelKey(evt, this.keyCode, this.target);
}

}

function cancelKey(evt, keyCode, target) {

// Ovaj deo je za budzenje ako treba zabraniti i <- i return van forme!
//if (keyCode==8 || keyCode==13) {
// Don't want to disable Backspace or Enter in text fields
//if (target.type == "text" || target.type == "textarea") {
//return true;
//}
//}

if (evt.preventDefault) {
evt.preventDefault();
evt.stopPropagation();
} else {
evt.keyCode = 0;
evt.returnValue = false;
}
return false;
}
// ============= DisableKeys.js =============//