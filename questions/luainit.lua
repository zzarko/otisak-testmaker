-- return character that corresponds to number
function digit(x)
    if x < 10 then
        return string.char(48+x)
    else
        return string.char(55+x)
    end
end

-- converts number to string in given base, with zeros padding
-- number, base, zeros padding
function base(x,b,p)
    ret=""
    while x >= b do
        ret = digit(x%b) .. ret
        x = math.modf(x/b)
    end
    ret = digit(x) .. ret
    while string.len(ret) < p do
        ret = "0" .. ret
    end
    return ret
end

-- just short for math.random
function rnd(x)
    return math.random(x)
end

-- initialize random number generator
math.randomseed(os.time())

