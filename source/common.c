/*
    This file is part of Testmaker.
    Testmaker (c) 2007,2015 Žarko Živanov

    Testmaker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LUA is published under MIT licence, see licence.txt in LUA directory
*/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <ctype.h>

#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/stat.h>

#include "defs.h"
#include "common.h"
#include "cparser.tab.h"

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#ifdef __DEBUG__
int DebugTrace = 0;
#endif

/* the Lua interpreter */
lua_State* L;
Tlua* luacode = 0;
int has_lua = 0;
Tluascr* luascripts = 0;

//bafer za ispisivanje gresaka
char errbuff[1024];

//lista iskoristenih kombinacija pitanja
Tusedq* usedq = 0;
Tusedq* lastusedq = 0;

//lista iskoristenih grupa pitanja
Tusedgs* usedgs = 0;
Tusedg* lastusedg = 0;

//odt odgovori
Todtanswers odtanswers;
extern int odt_qnumber;
extern char* odtcorrect;

//konverzija znakova
Ttranscode_tbl transcode_tbl;

extern int error_count;
extern int print_tanswers;
extern int default_edit_filed;
extern int all_answers_wrong;
extern Tchapters chapters;
extern FILE* cyyin;
extern FILE* radio;
extern FILE* check;
extern FILE* freetext;
extern FILE* odt;
extern char course[];
extern char tcurrent_file[];
FILE* errors;
extern int scaling;

//zauzima memoriju i inicijalizuje je na 0
void* malloc0(size_t size) {
    void *p;
    if ((p  = malloc(size))== NULL) {
        cyyerror("\nNot enough heap memory!!!");
        exit(1);
    }
    memset(p,0,size);
    return p;
}

//kopira jedan string u drugi i stavlja NULL na kraj
char* strncpy0(char *dest, const char *src, size_t maxlen) {
    strncpy(dest, src, maxlen);
    dest[maxlen] = 0;
    return dest;
}

//zamenjuje jedan podstring drugim
int string_replace(char** String, char* ReplaceWhat, char* ReplaceTo) {
    char *NewString, *c, *lastf;
    char *found = *String;
    int count = 0;
    int RepToLen = strlen(ReplaceTo);
    int RepWhatLen = strlen(ReplaceWhat);

    while (found) {
        if ((found = strstr(found, ReplaceWhat)) != 0) {
            found += RepWhatLen;
            count++;
        }
    }
    if (count) {
        c = NewString = malloc0(strlen(*String) + count*(RepToLen - RepWhatLen) + 1);
        lastf = *String;
        while ((found = strstr(lastf, ReplaceWhat)) != 0) {
            if (found != lastf) {
                strncpy(c, lastf, found-lastf);
                c += found-lastf;
            }
            strncpy(c, ReplaceTo, RepToLen);
            c += RepToLen;
            lastf = found+RepWhatLen;
        }
        strcpy(c, lastf);
        free(*String);
        *String = NewString;
    }
    return count;
}

//konvertuje vodece praznine u nbsp
void leading_space_nbsp(char** s) {
    int n,ch;
    int i;
    char *ns;
    for (i=0,n=0,ch=0;isspace((*s)[i]);i++)
        switch ((*s)[i]) {
            case ' '  : n++;
                        ch++;
                        break;
            case '\t' : n+=TAB_SIZE;
                        ch++;
                        break;
        }
    ns=(char*)malloc0(strlen(*s)+n*strlen(HTML_NBSP)-ch+1);
    for (i=0;i<n;i++) strcat(ns,HTML_NBSP);
    strcat(ns,(*s)+ch);
    free(*s);
    *s = ns;
}

//uklanjanje razmaka sa pocetka i kraja stringa
int trim_space(char* s) {
    int leading = 0;
    char *p = s;
    if (!s) return 0;
    while (*p) p++;
    while (isspace(*(p-1))) p--;
    *p = 0;
    while (isspace(*s)) {
        p = s;
        while (*(p+1)) {
            *p = *(p + 1);
            p++;
        }
        *p = 0;
        leading++;
    }
    return leading;
}

//konvertuje problematicne znake u odgovarajuci HTML kod
void fix_html_tags(char** s) {
    string_replace(s,"&","&amp;");
    string_replace(s,"\"","&amp;quot;");
    string_replace(s,"'","&amp;#39;");
    string_replace(s,"[","&amp;#91;");
    string_replace(s,"]","&amp;#93;");
    string_replace(s,"<i>","[i]");
    string_replace(s,"</i>","[/i]");
    string_replace(s,"<b>","[b]");
    string_replace(s,"</b>","[/b]");
    string_replace(s,"<c>","[code]");
    string_replace(s,"</c>","[/code]");
    string_replace(s,"\\<","&amp;lt;");
    string_replace(s,"\\>","&amp;gt;");
    string_replace(s,"<","&amp;lt;");
    string_replace(s,">","&amp;gt;");
    string_replace(s,"\\I","[i]");
    string_replace(s,"\\i","[/i]");
    string_replace(s,"\\B","[b]");
    string_replace(s,"\\b","[/b]");
    string_replace(s,"\\C","[code]");
    string_replace(s,"\\c","[/code]");
    string_replace(s,"\\","&amp;#92;");
}

//konvertuje problematicne znake u odgovarajuci ODT XML kod
void fix_odt_tags(char** s) {
    char *spaces = strdup("                                                       ");
    int slen = strlen(spaces);
    char rto[strlen(XML_ODT_TABBEGIN)+strlen(XML_ODT_TABEND)+5];

    string_replace(s,"&","&amp;");
    string_replace(s,"<","&lt;");
    string_replace(s,">","&gt;");
    string_replace(s,"&lt;i&gt;","<text:span text:style-name=\"T2\">");
    string_replace(s,"&lt;/i&gt;","</text:span>");
    string_replace(s,"&lt;b&gt;","<text:span text:style-name=\"T1\">");
    string_replace(s,"&lt;/b&gt;","</text:span>");
    string_replace(s,"&lt;c&gt;","<text:span text:style-name=\"Code\">");
    string_replace(s,"&lt;/c&gt;","</text:span>");
    string_replace(s,"\\&lt;","&lt;");
    string_replace(s,"\\&gt;","&gt;");
    string_replace(s,"\\I","<text:span text:style-name=\"T2\">");
    string_replace(s,"\\i","</text:span>");
    string_replace(s,"\\B","<text:span text:style-name=\"T1\">");
    string_replace(s,"\\b","</text:span>");
    string_replace(s,"\\C","<text:span text:style-name=\"Code\">");
    string_replace(s,"\\c","</text:span>");

    while (slen > 1) {
        if (strstr(*s,spaces)) {
            sprintf(rto,"%s%d%s",XML_ODT_TABBEGIN,slen,XML_ODT_TABEND);
//            printf("FROM:[%s] TO:[%s]\n",spaces,rto);
            string_replace(s,spaces,rto);
        }
        spaces[--slen] = 0;
    }
}


//da li je trenutno generisanje teksta sa kodom
int code_style = 0;

//dodavanje nove linije u tekst pitanja
char* add_line(char** text, char** otext, char* line) {
    int lt = 0;
    int ll = 0;
    char* s;
    char* prefix="";
    char* postfix="";
    char* oprefix1="";
    char* oprefix;
    char* opostfix;

    extract_lua_code(&line);

    char* oline = strdup(line);
    char* otabs = (char*)malloc0(strlen(XML_ODT_TABBEGIN)+strlen(XML_ODT_TABEND)+4);

    fix_html_tags(&line);
    fix_odt_tags(&oline);
    //ako je prvi znak razmak, liniju tretirati kao programski kod
    if (isspace(line[0])) {
        if (!code_style) {
            code_style=1;
            prefix=(char*)malloc(strlen(HTML_CODE_START)+1);
            strcpy(prefix,HTML_CODE_START);
            oprefix1 = strdup(XML_ODT_CODE_BEGIN);
        }
        oprefix = strdup(XML_ODT_CBEGIN);
        opostfix = strdup(XML_ODT_CEND);
        leading_space_nbsp(&line);
        string_replace(&line, " ",HTML_NBSP);
    }
    //ako linija ne pocinje razmakom, tekst nadalje ne tretirati kao programski kod
    else {
        if (code_style) {
            code_style=0;
            prefix=(char*)malloc(strlen(HTML_CODE_END)+1);
            strcpy(prefix,HTML_CODE_END);
            oprefix1 = strdup(XML_ODT_CODE_END);
        }
        oprefix = strdup(XML_ODT_TBEGIN);
        opostfix = strdup(XML_ODT_TEND);
    }

    if (*otext) lt = strlen(*otext);
    if((ll = trim_space(oline)) != 0)
        sprintf(otabs,"%s%d%s",XML_ODT_TABBEGIN,ll,XML_ODT_TABEND);
    else
        otabs[0]=0;
    s = (char*)malloc0(lt+strlen(oline)+strlen(oprefix1)+strlen(oprefix)+strlen(opostfix)+strlen(otabs)+2);
    if (*otext) {
        strcpy(s,*otext);
//        strcat(s, "\n");
        free(*otext);
    }
    strcat(s,oprefix1);
    if (strlen(oline)) {
        strcat(s,oprefix);
        strcat(s,otabs);
        strcat(s,oline);
        strcat(s,opostfix);
    }

    *otext = s;
    free(oline);
    if (strlen(oprefix1)) free(oprefix1);
    free(oprefix);
    free(opostfix);

    trim_space(line);
    ll = strlen(line);
    if (*text) lt = strlen(*text);
    else lt = 0;
    s = (char*)malloc0(lt+ll+strlen(HTML_SPACES)+strlen(HTML_NL)+strlen(prefix)+strlen(postfix)+1);
    //ako vec postoji neki tekst, dodaj na njega
    if (*text) {
        strcpy(s,*text);
        if (strlen(prefix)) strcat(s, "\n");
        else strcat(s, HTML_NL);
        free(*text);
    }
    strcat(s,HTML_SPACES);
    strcat(s,prefix);
    strcat(s,line);
    strcat(s,postfix);
    *text = s;
    free(line);
    if (strlen(prefix)) free(prefix);

    return s;
}

//dodavanje nove oblasti u listu oblasti
Tchapter* insert_chapter(Tchapters* s) {
    Tchapter* c = (Tchapter*)malloc0(sizeof(Tchapter));
    if (!s->chapter) s->chapter = s->lastc = c;
    else {
        s->lastc->next = c;
        s->lastc = c;
    }
    return c;
}

//provera da li id poslednje dodatog pitanja vec postoji u listi
void check_qid(Tchapter* c) {
    Tquestion* q;
    q = c->question;
    while (q!=c->lastq) {
        if (strcmp(q->id,c->lastq->id)==0) {
            scyyerror("Question id %s already defined in line %d",q->id,q->line);
            break;
        }
        q = q->next;
    }
}

//dodavanje novog pitanja u listu pitanja
Tquestion* insert_question(Tchapter* c) {
    Tquestion* q = (Tquestion*)malloc0(sizeof(Tquestion));
    if (!c->question) c->question = c->lastq = q;
    else {
        c->lastq->next = q;
        c->lastq = q;
    }
    return q;
}

//dodavanje novog bloka u listu blokova
Tblock* insert_block(Tquestion* q) {
    Tblock* b = (Tblock*)malloc0(sizeof(Tblock));
    if (!q->block) q->block = q->lastb = b;
    else {
        q->lastb->next = b;
        q->lastb = b;
    }
    return b;
}

/*
void delete_block(Tblock* b) {
    int i;
    if (b->text) free(b->text);
    if (b->tanswer[0]) free(b->tanswer[0]);
    for (i = 0; i < b->nanswers; i++) if (b->nanswer[i]) free(b->nanswer[i]);
    free(b);
}

void delete_question(Tquestion* q) {
    if (q->id) free(q->id);
    if (q->block) {
        Tblock* b = q->block;
        while (b) {
            q->block = b->next;
            delete_block(b);
            b = q->block;
        }
    }
    free(q);
}

void delete_chapter(Tchapter* c) {
    if (c->name) free(c->name);
    if (c->question) {
        Tquestion* q = c->question;
        while (q) {
            c->question = q->next;
            delete_question(q);
            q = c->question;
        }
    }
    free(c);
}
*/

void add_odt_answer(char* a, int c) {
    odtanswers.answer[odtanswers.answers] = a;
    odtanswers.correct[odtanswers.answers] = c;
    odtanswers.answers++;
}

void write_odt_answers(int qtype, int posfound, char* pointp) {
    int i,n,i1,i2,c;
    char* s;
    char correct[256] = "";
    char ans[2] = "";

    //randomize
    n = 1 + rand() % 5;
    for (i=0; i < n; i++) {
        i1 = rand() % odtanswers.answers;
        i2 = rand() % odtanswers.answers;
        s = odtanswers.answer[i1];
        odtanswers.answer[i1] = odtanswers.answer[i2];
        odtanswers.answer[i2] = s;
        c = odtanswers.correct[i1];
        odtanswers.correct[i1] = odtanswers.correct[i2];
        odtanswers.correct[i2] = c;
    }

    if ((all_answers_wrong == 2)&&( (qtype==_Q_POSITIVE)||(qtype==_Q_NEGATIVE) )) {
        add_odt_answer(strdup(XML_ODT_ALL_ANSWERS), posfound ? 0 : 1);
    }

    //write
    for (i=0; i < odtanswers.answers; i++) {
        ofprintf(odt,"%s%s%s",XML_ODT_ABEGIN,odtanswers.answer[i],XML_ODT_AEND);
        if (odtanswers.correct[i]) {
            if (strlen(correct))
                strcat(correct,",");
            sprintf(ans,"%c",'a'+i);
            strcat(correct,ans);
        }
    }
    add_odt_correct(correct, pointp);
}

void write_odt_textline(char* text) {
    char *s1 = strdup(text);
    char *s2 = strdup(XML_ODT_TEXTLINE);
    fix_odt_tags(&s1);
    string_replace(&s2,"$$$",s1);
    ofprintf(odt, "%s",s2);
}

void add_odt_emptyline(char** otext) {
    char *s;
    int lt = 0;
    if (*otext) lt = strlen(*otext);
    s = (char*)malloc0(lt+strlen(XML_ODT_EMPTYLINE)+1);
    if (*otext) {
        strcpy(s,*otext);
        free(*otext);
    }
    strcat(s,XML_ODT_EMPTYLINE);
    *otext = s;
}

void add_odt_correct(char* c, char* pointp) {
    char *s, n[10];
    int lt = 0;
    if (odtcorrect) lt = strlen(odtcorrect);
    s = (char*)malloc0(lt+strlen(c)+strlen(XML_ODT_CORRECT_BEGIN)+strlen(XML_ODT_CORRECT_END)+15);
    if (odtcorrect) {
        strcpy(s,odtcorrect);
        free(odtcorrect);
    }
    sprintf(n,"%d – ",odt_qnumber);
    strcat(s,XML_ODT_CORRECT_BEGIN);
    strcat(s,n);
    strcat(s,c);
    sprintf(n," (%.2g)",atoi(pointp)*1.0/scaling);
    strcat(s,n);
    strcat(s,XML_ODT_CORRECT_END);
    odtcorrect = s;
}

//generisanje radio button XML koda
void make_xml_rquestion(FILE* out, Tquestion* q, int ex, char* pointp, char* pointn) {
    Tblock* b = q->block;
    int first_block = 1;
    char* buff;
    int i;
    int positive_found = 0;

    tfprintf(out,"%s",XML_RADIO_QBEGIN);
    while(b) {
        tfprintf(out,"%s",XML_RADIO_BBEGIN);

        tfprintf(out,"%s",XML_RADIO_TBEGIN);
        if (first_block) {
            if (q->type==_Q_POSITIVE) tfprintf(out,"%s",XML_RADIO_PFIRSTTEXTLINE);
            else tfprintf(out,"%s",XML_RADIO_NFIRSTTEXTLINE);
            first_block = 0;
        }
        tfprintf(out,"%s",b->text);
        tfprintf(out,"%s",XML_RADIO_TEND);

        tfprintf(out,"%s",XML_RADIO_PBEGIN);
        if (b->picture) {
            char dir1[1024];
            char dir2[1024];

            tfprintf(out,"%s",b->picture);
            //kopiranje slike u images direktorijum tekuceg testa
            sprintf(dir1,"%s",DIR_IMAGES);
            sprintf(dir2,"%s/%s/%s/images",DIR_OUTPUT,tcurrent_file,course);
            copyfile(b->picture, dir1, dir2);
        }
        tfprintf(out,"%s",XML_RADIO_PEND);

        ofprintf(odt,"%s",XML_ODT_ALISTBEGIN);
        odtanswers.answers = 0;

        if (ex && b->tanswer[0]) {
            buff=strdup(XML_RADIO_ABEGIN);
            string_replace(&buff,"$$$",pointp);
            tfprintf(out,"%s",buff);
            free(buff);
            tfprintf(out,"%s",b->tanswer[0]);
            add_odt_answer(b->otanswer[0], 1);
            tfprintf(out,"%s",XML_RADIO_AEND);
            positive_found = 1;
        }
        for (i = 0; i < b->nanswers; i++)
            if (i != ex-1) {
                buff=strdup(XML_RADIO_ABEGIN);
                string_replace(&buff,"$$$",pointn);
                tfprintf(out,"%s",buff);
                free(buff);
                tfprintf(out,"%s",b->nanswer[i]);
                add_odt_answer(b->onanswer[i], 0);
                tfprintf(out,"%s",XML_RADIO_AEND);
            }
        tfprintf(out,"%s",XML_RADIO_BEND);
        write_odt_answers(q->type, ex && positive_found, pointp);
        ofprintf(odt,"%s",XML_ODT_ALISTEND);
        b = b->next;
    }
    if (all_answers_wrong) {
        if (q->type==_Q_POSITIVE) buff=strdup(XML_RADIO_PLASTBLOCK);
        else buff=strdup(XML_RADIO_NLASTBLOCK);
        if (ex && positive_found) string_replace(&buff,"$$$",pointn);
        else string_replace(&buff,"$$$",pointp);
        tfprintf(out,"%s",buff);
        free(buff);
    }
    tfprintf(out,"%s",XML_RADIO_LASTBLOCK1);
    if (atoi(pointn)<0)
        tfprintf(out,"%s",XML_RADIO_REMARK);
    tfprintf(out,"%s",XML_RADIO_LASTBLOCK2);
    tfprintf(out,"%s",XML_RADIO_NOANSWER);
    if (q->editfield || default_edit_filed)
        tfprintf(out,"%s",XML_EDIT_FIELD);
    tfprintf(out,"%s",XML_RADIO_QEND);
}

//generisanje checkbox XML koda
void make_xml_cquestion(FILE* out, Tquestion* q, char* pointp, char* pointn) {
    Tblock* b = q->block;
    char* buff;
    int i,p,num_p;
    double dp,int_part,frac_part;
    char positive[32],tanswers[32];
//    int positive_found = 0;
    int tansw = 0;

    tfprintf(out,"%s",XML_CHECK_QBEGIN);
    p = atoi(pointp);
    num_p = 0;
    while (b) {
        num_p = num_p + b->tanswers;
        b = b->next;
    }
    if (num_p == 0) num_p = 1;
    dp = (p*1.0)/num_p;
    frac_part = modf(dp,&int_part);
    if (frac_part > 0.01) int_part = int_part + 1;
    sprintf(positive,"%ld",(long int)(int_part + 0.5));
    b = q->block;
    while(b) {
        tfprintf(out,"%s",XML_CHECK_BBEGIN);
        tfprintf(out,"%s",XML_CHECK_TBEGIN);
        tfprintf(out,"%s",XML_CHECK_FIRSTTEXTLINE);
        tfprintf(out,"%s",b->text);
        tansw += b->tanswers;
        tfprintf(out,"%s",XML_CHECK_TEND);

        tfprintf(out,"%s",XML_CHECK_PBEGIN);
        if (b->picture) {
            char dir1[1024];
            char dir2[1024];

            tfprintf(out,"%s",b->picture);
            //kopiranje slike u images direktorijum tekuceg testa
            sprintf(dir1,"%s",DIR_IMAGES);
            sprintf(dir2,"%s/%s/%s/images",DIR_OUTPUT,tcurrent_file,course);
            copyfile(b->picture, dir1, dir2);
        }
        tfprintf(out,"%s",XML_CHECK_PEND);

        ofprintf(odt,"%s",XML_ODT_ALISTBEGIN);
        odtanswers.answers = 0;

        for (i = 0; i < b->tanswers; i++) {
            buff=strdup(XML_CHECK_ABEGIN);
            string_replace(&buff,"$$$",positive);
            tfprintf(out,"%s",buff);
            free(buff);
            tfprintf(out,"%s",b->tanswer[i]);
            add_odt_answer(b->otanswer[i], 1);
            tfprintf(out,"%s",XML_CHECK_AEND);
//            positive_found = 1;
        }
        for (i = 0; i < b->nanswers; i++) {
            buff=strdup(XML_CHECK_ABEGIN);
            string_replace(&buff,"$$$",pointn);
            tfprintf(out,"%s",buff);
            free(buff);
            tfprintf(out,"%s",b->nanswer[i]);
            add_odt_answer(b->onanswer[i], 0);
            tfprintf(out,"%s",XML_CHECK_AEND);
            }
        tfprintf(out,"%s",XML_CHECK_BEND);
        write_odt_answers(q->type, 1, pointp);
        ofprintf(odt,"%s",XML_ODT_ALISTEND);
        b = b->next;
    }
    tfprintf(out,"%s",XML_CHECK_LASTBLOCK1);
    if (atoi(pointn)<0)
        tfprintf(out,"%s",XML_CHECK_REMARK1);
    else
        tfprintf(out,"%s",XML_CHECK_REMARK2);
    if (print_tanswers) {
        sprintf(tanswers,"%d",tansw);
        buff=strdup(XML_CHECK_LASTBLOCK3);
        string_replace(&buff,"$$$",tanswers);
        tfprintf(out,"%s",buff);
        buff=strdup(XML_ODT_CNUMBER);
        string_replace(&buff,"$$$",tanswers);
        ofprintf(odt,"%s",buff);
        free(buff);
    }
    else
        tfprintf(out,"%s",XML_CHECK_LASTBLOCK2);
    if (q->editfield || default_edit_filed)
        tfprintf(out,"%s",XML_EDIT_FIELD);
    tfprintf(out,"%s",XML_CHECK_QEND);
}

//generisanje freetext XML koda
void make_xml_tquestion(FILE* out, Tquestion* q, char* pointp, char* pointn) {
    Tblock* b = q->block;
    char* buff;

    buff=strdup(XML_TEXT_QBEGIN);
    string_replace(&buff,"$$$",pointn);
    tfprintf(out,"%s",buff);
    free(buff);

   while (b) {
        tfprintf(out,"%s",XML_TEXT_BBEGIN);

        tfprintf(out,"%s",XML_TEXT_TBEGIN);
        tfprintf(out,"%s",b->text);
        tfprintf(out,"%s",XML_TEXT_TEND);

        tfprintf(out,"%s",XML_TEXT_PBEGIN);
        if (b->picture) {
            char dir1[1024];
            char dir2[1024];

            tfprintf(out,"%s",b->picture);
            //kopiranje slike u images direktorijum tekuceg testa
            sprintf(dir1,"%s",DIR_IMAGES);
            sprintf(dir2,"%s/%s/%s/images",DIR_OUTPUT,tcurrent_file,course);
            copyfile(b->picture, dir1, dir2);
        }
        tfprintf(out,"%s",XML_TEXT_PEND);

        if (b->fttext) {
            tfprintf(out,"%s",XML_TEXT_HBEGIN);
            tfprintf(out,"%s",b->fttext);
            tfprintf(out,"%s",XML_TEXT_HEND);

            buff=strdup(XML_TEXT_ABEGIN);
            string_replace(&buff,"$$$",pointp);
            tfprintf(out,"%s",buff);
            free(buff);
            tfprintf(out,"%s",b->ftanswer);
            tfprintf(out,"%s",XML_TEXT_AEND);
            add_odt_correct(b->ftanswer, pointp);
        }

        ofprintf(odt,"%s",XML_ODT_ANSWER);
        //TODO:zapamti tacan odgovor
        tfprintf(out,"%s",XML_TEXT_BEND);
        b = b->next;
    }
    tfprintf(out,"%s",XML_TEXT_LASTBLOCK1);
    if (atoi(pointn)<0)
        tfprintf(out,"%s",XML_TEXT_REMARK);
    tfprintf(out,"%s",XML_TEXT_LASTBLOCK2);
    if (q->editfield || default_edit_filed)
        tfprintf(out,"%s",XML_EDIT_FIELD);
    tfprintf(out,"%s",XML_TEXT_QEND);
}

//generisanje XML koda za proizvoljno pitanje
int make_xml_question(FILE* out, char* ch, char* id, char* ts, int ex, char* pointp, char* pointn, int line) {
    Tchapter* c;
    Tquestion* q;
    Tusedg *g = 0, *ng;
    int used_print;
    int unused_count = 0;
    dprintf("\nts(%s) ch(%s) id(%s) +(%s) -(%s)",ts,ch,id,pointp,pointn);
    do {
        c = chapters.chapter;
        //trazenje oblasti u listi oblasti
        while(c) {
            if (strcmp(c->name,ch)==0) break;
            c = c->next;
        }
        //ako oblast nije nadjena, parsiraj odgovarajuci fajl
        if (c==0) {
            parse_file(ch);
            //ako fajl nije nadjen, povratak (odgovarajuca poruka o gresci je vec ispisana)
            if (!cyyin) return 0;
        }
    } while (c==0);
    q = c->question;
    //trazenje id-a pitanja
    while(q) {
        if (strcmp(q->id,id)==0)
            break;
        if (strcmp(q->gid,id)==0) {
            int notused = 1;
            Tusedgs *ugs = usedgs;
            Tusedg *ug;
            while (ugs && (strcmp(ugs->g->q->gid,q->gid) || strcmp(ugs->g->chapter,ch))) ugs = ugs->next;
            if (ugs) {
                ug = ugs->g;
                while (ug) {
                    //if ((strcmp(ug->test,ts) == 0) && (strcmp(ug->q->qid,gg->q->qid) == 0)) {
                    if ((strcmp(ug->q->qid,q->qid) == 0)) {
                        notused = 0;
                        break;
                    }
                    ug = ug->next;
                }
            }
            if (notused) {
                ng = (Tusedg*)malloc0(sizeof(Tusedg));
                ng->q = q;
                ng->next = g;
                g = ng;
                unused_count++;
            }
        }
        q = q->next;
    }

    if ( (q) && (q->luascr) )
        run_lua_script(q->luascr);

    if (g) {
        Tusedg *gg;
        int selected = rand() % unused_count;
        q = NULL;
        gg = g;

/*        {//debug ispis za nađena neiskorištena pitanja*/
/*            Tusedg *gselect = gg;*/
/*            printf("COUNT:%d\n",unused_count);*/
/*            while (gselect) {*/
/*                printf("ID:%s GID:%s QID:%s\n",gselect->q->id,gselect->q->gid,gselect->q->qid);*/
/*                gselect = gselect->next;*/
/*            }*/
/*        }*/

        while (gg && selected) {
            gg = gg->next;
            selected--;
        }
        if (gg) {
            q = gg->q;
            printf("\033[0;32mFor test %s (line %d), for group %s from file %s, question %s is chosen\033[0m\n",ts,line,id,ch,q->qid);
        }

        while (g) {
            gg = g;
            g = g->next;
            free(gg);
        }
        if (q == NULL) {
            styyerror("No more unused questions in group %s from file %s.txt !",id,ch);
            return 0;
        }
    }
    if (q) {
        Tusedq *uq;
        Tusedg *ug,*nug;
        Tusedgs *ugs,*nugs;
        int sameid;
        
        ofprintf(odt,"%s",XML_ODT_QBEGIN);
        ofprintf(odt,"%s",q->block->otext);

        if ((q->type==_Q_POSITIVE) || (q->type==_Q_NEGATIVE))
            make_xml_rquestion(radio,q,ex,pointp,pointn);
        else if (q->type == _Q_CHECKBOX) {
            make_xml_cquestion(check,q,pointp,pointn);
            ex = 0;
        }
        else {
            make_xml_tquestion(freetext,q,pointp,pointn);
            ex = 0;
        }

        ofprintf(odt,"%s",XML_ODT_QEND);

        //pamcenje u listi iskoristenih pitanja
        uq = (Tusedq*)malloc0(sizeof(Tusedq));
        uq->q = q;
        uq->ex = ex;
        if (q->luaid) {
            uq->luaid = strdup(q->luaid);
            lua_transform(&(uq->luaid));
        }
        uq->test = strdup(ts);
        uq->line = line;
        if (!usedq) usedq = lastusedq = uq;
        else {
            lastusedq->next = uq;
            lastusedq = uq;
        }
        //provera da li je pitanje vec bilo (bez posebne provere grupe)
        uq = usedq;
        used_print = 0;
        while (uq!=lastusedq) {
            sameid = 1;
            if ((uq->luaid) && (lastusedq->luaid))
                sameid = strcmp(uq->luaid,lastusedq->luaid) == 0;
            if ( (uq->q==lastusedq->q) && (uq->ex==lastusedq->ex) && sameid ) {
                stwarning("Question already used in test %s in line %d%s",uq->test,uq->line,sameid?" (same LUA ID)":"");
                used_print = 1;
                break;
            }
            uq = uq->next;
        }
        //provera da li je grupa pitanja vec bila u tekucem testu
        nug = (Tusedg*)malloc0(sizeof(Tusedg));
        nug->q = q;
        nug->chapter = strdup(ch);
        nug->test = strdup(ts);
        nug->line = line;
        lastusedg = nug;
        if (!usedgs) {
            usedgs = (Tusedgs*)malloc0(sizeof(Tusedgs));
            usedgs->g = nug;
        }
        else {
            ugs = usedgs;
            while (ugs && (strcmp(ugs->g->q->gid,q->gid) || strcmp(ugs->g->chapter,ch))) ugs = ugs->next;
            if (ugs) {
                ug = ugs->g;
                while (ug) {
                    if (strcmp(ug->test,ts) == 0) {
                        if ( (strcmp(q->qid,"")==0) && (!q->has_lua) ) {
                            if (!used_print) stwarning("Question already used in different form in line %d",ug->line);
                        } else if (!q->has_lua)
                            stwarning("Group %s from file %s already used in line %d",q->gid,ch,ug->line);
                        break;
                    }
                    ug = ug->next;
                }
                nug->next = ugs->g;
                ugs->g = nug;
            } else {
                nugs = (Tusedgs*)malloc0(sizeof(Tusedgs));
                nugs->g = nug;
                nugs->next = usedgs;
                usedgs = nugs;
            }
        }
        return 1;
    }
    else {
        styyerror("Question ID %s in file %s.txt not found!",id,ch);
        return 0;
    }
}

//inicijalizacija transcode funkcije, tj. niza transcode_tbl
void init_transcode(char* transcode_file) {
    FILE* tc;
    char fname[512];

    transcode_tbl.n = 0;
    sprintf(fname,"%s/%s.txt",DIR_TRANSCODE,transcode_file);
    if (!(tc = fopen(fname,"r"))) {
        sprinterror("Transcode file %s.txt not found!",transcode_file);
        exit(1);
    }
    while (!feof(tc)) {
        char* buff;
        int n;
        size_t ng;

        if (transcode_tbl.n >= MAX_TRANSCODE) {
            sprinterror("More than %d entries in transcode.txt! (extra entries truncated)",MAX_TRANSCODE);
            return;
        }
        buff = 0;
        //parsiranje linije i razdvajanje sta se u sta konvertuje
        if (getline(&buff,&ng,tc)>0) {
            trim_space(buff);
            if (strlen(buff)>0) {
                n = strcspn(buff," \t");
                transcode_tbl.from[transcode_tbl.n] = strndup(buff,n);
                trim_space(transcode_tbl.from[transcode_tbl.n]);
                transcode_tbl.to[transcode_tbl.n] = strdup(buff+n);
                trim_space(transcode_tbl.to[transcode_tbl.n]);
                transcode_tbl.n++;
            }
        }
        if (buff) free(buff);
    }
}

//konvertovanje nasih kuka i kvaka u odgovarajuce kodove
void transcode(char** s) {
    int i;
    for (i = 0; i < transcode_tbl.n; i++)
        string_replace(s,transcode_tbl.from[i],transcode_tbl.to[i]);
}

//funkcija koja zamenjuje fprintf, s tim da pre upisa u fajl pozove transcode i LUA interpreter
int tfprintf(FILE *stream, const char *format, ...) {
    size_t size;
    size_t sizem=1024;
    va_list ap,apc;
    char* buff;
    int ret;

    va_start(ap,format);
    do {
        buff = (char*)malloc(sizem);
        va_copy(apc, ap);
        size = vsnprintf(buff, sizem, format, apc);
        va_end(apc);
        if (size<sizem) break;
        else {
            dprintf("small buffer %d->%d\n",sizem,size+1);
            free(buff);
            sizem = size+1;
        }
     } while (1);
    va_end(ap);

    lua_transform(&buff);

    transcode(&buff);
    ret=fprintf(stream,"%s",buff);
    free(buff);
    return ret;
}

//funkcija koja zamenjuje fprintf za odt fajl
int ofprintf(FILE *stream, const char *format, ...) {
    size_t size;
    size_t sizem=1024;
    va_list ap,apc;
    char* buff;
    int ret;

    va_start(ap,format);
    do {
        buff = (char*)malloc(sizem);
        va_copy(apc, ap);
        size = vsnprintf(buff, sizem, format, apc);
        va_end(apc);
        if (size<sizem) break;
        else {
            dprintf("small buffer %d->%d\n",sizem,size+1);
            free(buff);
            sizem = size+1;
        }
     } while (1);
    va_end(ap);

    lua_transform(&buff);

    dprintf("\nODT:{%s}",buff);
    ret=fprintf(stream,"%s",buff);
    free(buff);
    return ret;
}

//provera tagova u tekstu
int check_tags(char *s) {
    int i,t,italic=0,bold=0,code=0,italic1=0,bold1=0,code1=0,len=strlen(s);
//    char *tags[]={"<i>","</i>","\\I","\\i","[i]","[/i]","<b>","</b>","\\B","\\b","[b]","[/b]","<c>","</c>","\\C","\\c","[code]","[/code]"};
    char *tags[]={"<i>","</i>","\\I","\\i","<i>","</i>","<b>","</b>","\\B","\\b","<b>","</b>","<c>","</c>","\\C","\\c","<c>","</c>"};
    int tagl[18];
    for (t=0;t<18;t++) tagl[t] = strlen(tags[t]);

    for (i=0;i<len;i++) {
        for (t=0;t<18;t++)
            if (strncmp(&s[i],tags[t],tagl[t]) == 0) {
                switch (t) {
                    case  0: case  2: case  4: italic++; break;
                    case  1: case  3: case  5: italic--; break;
                    case  6: case  8: case 10: bold++; break;
                    case  7: case  9: case 11: bold--; break;
                    case 12: case 14: case 16: code++; break;
                    case 13: case 15: case 17: code--; break;
                    default: printerror("BUG in check_tags");
                }
                if (italic != italic1) {
                    if (italic<0) scyyerror("Italic tag closed, but not opened");
                    if (italic>1) scyyerror("Italic tag opened more than once");
                    italic1 = italic;
                }
                if (bold != bold1) {
                    if (bold<0) scyyerror("Bold tag closed, but not opened");
                    if (bold>1) scyyerror("Bold tag opened more than once");
                    bold1 = bold;
                }
                if (code != code1) {
                    if (code<0) scyyerror("Code tag closed, but not opened");
                    if (code>1) scyyerror("Code tag opened more than once");
                    code1 = code;
                }
                break;
            }
    }
/*    if (italic<0) scyyerror("Italic tag closed, but not opened");
    if (bold<0) scyyerror("Bold tag closed, but not opened");
    if (code<0) scyyerror("Code tag closed, but not opened");*/
    if (italic==1) scyyerror("Italic tag opened, but not closed");
    if (bold==1) scyyerror("Bold tag opened, but not closed");
    if (code==1) scyyerror("Code tag opened, but not closed");
    return (italic||bold||code);
}

//provera identicnog teksta odgovora
int check_repeating(Tblock* block, int tans) {
    int n,t,i,rep=0;
    char *ans;
    if (tans) {
        n = block->nanswers-1;
        t = block->tanswers-2;
        ans=block->tanswer[block->tanswers-1];
    } else {
        n = block->nanswers-2;
        t = block->tanswers-1;
        ans=block->nanswer[block->nanswers-1];
    }
    for (i=0; i<=n; i++)
        if (strcmp(ans, block->nanswer[i]) == 0) {
            scyyerror("Answer text already exists in question");
            rep = 1;
        }
    for (i=0; i<=t; i++)
        if (strcmp(ans, block->tanswer[i]) == 0) {
            scyyerror("Answer text already exists in question");
            rep = 1;
        }
    return rep;
}

void printerror(char *s) {
  fprintf(stderr, "\033[1;31mERROR: %s\033[0m\n", s);
  fprintf(errors, "ERROR: %s\n", s);
  error_count++;
}

//kopira fajl f iz direktorijuma din u direktorijum dout
//!pogledati da li ima neka standardna funkcija koja radi kopiranje
int copyfile(char *f, char *din, char *dout) {
    char errstr[1024];
    char inname[1024];
    char outname[1024];
    int nread, in, out, result = 0;
    const int BlockSize = 1024;
    char block[BlockSize];

    sprintf(inname,"%s/%s",din,f);
    sprintf(outname,"%s/%s",dout,f);
    if ((in = open(inname,O_RDONLY)) == -1) {
        sprintf(errstr,"Picture %s not found in %s directory",f,din);
        twarning(errstr);
    }
    else {
        if ((out = open(outname,O_WRONLY|O_CREAT,S_IWUSR)) == -1) {
            sprintf(errstr,"Unable to create %s\%s",dout,f);
            twarning(errstr);
        }
        else {
            while ( ( nread = read(in, block, BlockSize )) > 0) write(out, block, nread);
            close(out);
            chmod(outname,0664);
            result = 1;
        }
        close(in);
    }
    return result;
}

//kopira sadrzaj jednog direktorijuma u drugi
int copydir(char *din, char *dout) {
    DIR *dir;
    struct dirent *ent;
    char errstr[1024];
    int result = 1;

    if ((dir = opendir(din)) != 0) {
        while ((ent = readdir(dir)) != 0) {
            if ((ent->d_name)[0] != '.') {
                if (ent->d_type == DT_DIR) {
                    char d1[1024], d2[1024];
                    sprintf(d1,"%s/%s",din,ent->d_name);
                    sprintf(d2,"%s/%s",dout,ent->d_name);
                    mkdir(d2,(mode_t)0777);
                    copydir(d1,d2);
                }
                else
                    if (copyfile(ent->d_name, din, dout) == 0) result = 0;
            }
        }
        closedir(dir);
    }
    else {
        sprintf(errstr, "Unable to open %s directory", din);
        twarning(errstr);
        result = 0;
    }
    return result;
}

//brise sve fajlove iz zadatog direktorijuma
int cleardir(char *d) {
    DIR *dir;
    struct dirent *ent;
    char dname[1024];
    char errstr[1024];
    int result = 1;

    if ((dir = opendir(d)) != 0) {
        while ((ent = readdir(dir)) != 0) {
            if ((ent->d_name)[0] != '.') {
                sprintf(dname,"%s/%s",d,ent->d_name);
                if (ent->d_type == DT_DIR)
                    cleardir(dname);
                if ((remove(dname)) == -1) {
                    sprintf(errstr, "Unable to delete %s", dname);
                    twarning(errstr);
                    result = 0;
                }
            }
        }
        closedir(dir);
    }
    else {
        sprintf(errstr, "Unable to open %s directory", d);
        twarning(errstr);
        result = 0;
    }
    return result;
}

//nalazi prvi lua string u teklstu i vraća njegovu kopiju
char* get_first_lua(char* s) {
    char* begin = strstr(s, LUA_BEGIN);
    char* end = strstr(s, LUA_END);
    char* ret;
    int len;

    if ((begin==NULL)||(end==NULL)||(begin>end))
        return NULL;
    len = end - begin + 2;
    ret = malloc0(len + 3);
    strncpy(ret, begin, len);
    return ret;
}

//izdvaja lua kod iz teksta i zamenjuje ga ID-jevima koda
char* extract_lua_code(char** buff) {
    char* lua;
    char* luacmd;
    char luaid[32];
    int id=0,len=0;

    while ((lua = get_first_lua(*buff)) != NULL) {
        dprintf("\nFound LUA:%s\n", lua);
        len = strlen(lua) - strlen(LUA_BEGIN) - strlen(LUA_END);
        luacmd = strndup(lua+strlen(LUA_BEGIN), len);
        id = add_lua_code(luacmd);
        sprintf(luaid,"%s%d%s",LUA_RBEGIN, id, LUA_REND);
        string_replace(buff, lua, luaid);
        free(lua);
        has_lua = 1;
    }
    string_replace(buff, LUA_RBEGIN, LUA_BEGIN);
    string_replace(buff, LUA_REND, LUA_END);
    return *buff;
}

// dodaje lua kod u listu lua kodova i vraća ID koda
int add_lua_code(char *lua) {
    int id = 1;
    Tlua* lcode = malloc0(sizeof(Tlua));
    if (luacode) {
        id = luacode->id + 1;
        lcode->next = luacode;
        luacode = lcode;
    }
    else luacode = lcode;
    lcode->id = id;
    lcode->lua = lua;
    return id;
}

//nalazi lua kod preko ID-a
Tlua* find_lua_code(int id) {
    Tlua* lcode = luacode;
    while ((lcode) && (lcode->id != id)) lcode = lcode->next;
    if (lcode) return lcode;
    else return 0;
}

//pokreće lua kod sa zadatim ID-em i vraća rezultat izvršavanja
char* run_lua_code(int id) {
    char* lresult;
    int lerror;

    Tlua* lcode = find_lua_code(id);
    if (!lcode) {
        sprinterror("LUA ID NOT FOUND (shouldn't happen!!!): %d",id);
        return 0;
    }
    lerror = luaL_dostring(L,lcode->lua);
    if (lerror) {
        sprinterror("LUA: %s", lua_tostring(L, -1));
        return 0;
    }
    lresult = strdup(lua_tostring(L, -1));
    if (lcode->result) free(lcode->result);
    lcode->result = lresult;
    dprintf("\nLUA RESULT %d:%s\n",id,lresult);
    return lresult;
}

//vraća zatečeni rezultat za lua kod sa zadatim ID-em
//ako rezultat nije postavljen, izračuna ga
char* get_lua_result(int id) {
    Tlua* lcode = find_lua_code(id);
    if (lcode) {
        if (!lcode->result)
            return run_lua_code(id);
        else
            return lcode->result;
    }
    else return 0;
}

//menja tekst pozivanjem svih pojava lua koda u njemu
void lua_transform(char** buff) {
    int luaid;
    char* lua;
    char* lresult;

    while ((lua = get_first_lua(*buff)) != NULL) {
        luaid = atoi(lua+strlen(LUA_BEGIN));
        dprintf("\nLUA [%d]:%s\n",luaid, lua);
        lresult = get_lua_result(luaid);
        if (lresult)
            string_replace(buff,lua,lresult);
        else
            string_replace(buff,lua,"#LUAERROR#");
        free(lua);
    }
}

//briše sve rezultate izvršavanja lua koda
void clear_lua_results() {
    Tlua* lcode = luacode;
    while (lcode) {
        if (lcode->result) {
            free(lcode->result);
            lcode->result = 0;
        }
        lcode = lcode->next;
    }
}

//pokreće inicijalizacioni lua skript, ako već dotad nije pokrenut bez greške
int run_lua_script(char* name) {
    char lname[256];
    int lerror;
    Tluascr* scr;

    if (strstr(name,".lua") == 0)
        sprintf(lname,"%s/%s.lua", DIR_QUESTIONS, name);
    else
        sprintf(lname,"%s/%s", DIR_QUESTIONS, name);
    scr = luascripts;
    while (scr) {
        if (strcmp(lname,scr->script) == 0) return 0;
        scr = scr->next;
    }
    lerror = luaL_dofile(L,lname);
    if (lerror) {
        sprinterror("LUA: %s", lua_tostring(L, -1));
        return 1;
    }
    scr = malloc0(sizeof(Tluascr));
    scr->script = strdup(lname);
    scr->next = luascripts;
    luascripts = scr;
    return 0;
}

