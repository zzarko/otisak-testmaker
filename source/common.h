/*
    This file is part of Testmaker.
    Testmaker (c) 2007,2015 Žarko Živanov

    Testmaker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __COMMON_H__
#define __COMMON_H__

//jedan blok LUA koda
typedef struct _lua {
    char* lua;
    char* result;
    int error;
    int id;
    struct _lua* next;
} Tlua;

//jedan naziv LUA skripta
typedef struct _luascr {
    char* script;
    struct _luascr* next;
} Tluascr;

//opis jednog bloka u pitanju
typedef struct _block {
    char* text;
    char* otext;
    char* picture;
    int tanswers;
    char* tanswer[MAX_TANSWERS];
    char* otanswer[MAX_TANSWERS];
    int nanswers;
    char* nanswer[MAX_NANSWERS];
    char* onanswer[MAX_NANSWERS];
    char* fttext;
    char* ftanswer;
    struct _block* next;
} Tblock;

//opis pitanja
typedef struct _question {
    char* id;
    char* qid;
    char* gid;
    int type;
    int line;
    int editfield;
    int has_lua;
    char* luaid;
    char* luascr;
    Tblock* block;
    Tblock* lastb;
    struct _question* next;
} Tquestion;

//opis oblasti
typedef struct _chapter {
    char* name;
    Tquestion *question;
    Tquestion *lastq;
    struct _chapter* next;
} Tchapter;

//opis liste svih oblasti
typedef struct _chapters {
    Tchapter* chapter;
    Tchapter* lastc;
} Tchapters;

//za pamcenje koje su kombinacije pitanja vec bile
typedef struct _usedq {
    Tquestion* q;
    int ex;
    char* luaid;
    int line;
    char* test;
    struct _usedq* next;
} Tusedq;

//za pamcenje koje su grupe pitanja vec bile
typedef struct _usedg {
    Tquestion* q;
    int line;
    char* test;
    char* chapter;
    struct _usedg* next;
} Tusedg;

typedef struct _usedgs {
    Tusedg* g;
    struct _usedgs* next;
} Tusedgs;

//za pamcenje koji testovi su bili
typedef struct _usedt {
    char* test;
    int line;
    struct _usedt* next;
} Tusedt;

typedef struct _odtanswers {
    char* answer[MAX_TANSWERS+MAX_NANSWERS];
    int correct[MAX_TANSWERS+MAX_NANSWERS];
    int answers;
} Todtanswers;

typedef struct _transcode_tbl {
    char* from[MAX_TRANSCODE];
    char* to[MAX_TRANSCODE];
    int n;
} Ttranscode_tbl;

void* malloc0(size_t size);
int trim_space(char* s);
void fix_html_tags(char** s);
void init_transcode(char* transcode_file);
void transcode(char** s);
int tfprintf(FILE *stream, const char *format, ...);
int ofprintf(FILE *stream, const char *format, ...);
int check_tags(char *s);
int check_repeating(Tblock* block, int tans);
int string_replace(char** String, char* ReplaceWhat, char* ReplaceTo);
void printerror(char *s);

void fix_odt_tags(char** s);

char* add_line(char** text, char** otext, char* line);
int make_xml_question(FILE* out, char* ch, char* id, char* ts, int ex, char* pointp, char* pointn, int line);

Tchapter* insert_chapter(Tchapters* s);
Tquestion* insert_question(Tchapter* c);
Tblock* insert_block(Tquestion* q);
void check_qid(Tchapter* c);

int cyyerror(char *s);
void cwarning(char *s);
int tyyerror(char *s);
void twarning(char *s);
void parse_file(char* fname);

int copyfile(char *f, char *din, char *dout);
int copydir(char *din, char *dout);
int cleardir(char *d);

void add_odt_correct(char* c, char* pointp);
void add_odt_answer(char* a, int c);
//void write_odt_answers();
void add_odt_emptyline(char** otext);
void write_odt_textline(char* text);

char* get_first_lua(char* s);
char* extract_lua_code(char** buff);
int add_lua_code(char *lua);
Tlua* find_lua_code(int id);
char* run_lua_code(int id);
char* get_lua_result(int id);
void lua_transform(char** buff);
int run_lua_script(char* name);
void clear_lua_results();

extern FILE* errors;

#endif

