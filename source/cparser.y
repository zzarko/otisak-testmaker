/*
    This file is part of Testmaker.
    Testmaker (c) 2007,2015 Žarko Živanov

    Testmaker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

%{

//parser za fajlove sa tekstovima pitanja

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "defs.h"
#include "common.h"

int cyyparse(void);
int cyylex(void);
int cyyerror(char *s);

extern int cline;
extern FILE* cyyin;

extern void switch_to_file(char* fname);

int error_count = 0;
int warning_count = 0;

extern int has_lua;

int q_type = 0; //tip radio button pitanja
int make_new_block = 0; //indikacija da treba generisati novi blok u pitanju
int q_has_tanswer = 0;  //indikacija da je nadjen pozitivan odgovor
int q_has_freetext = 0; //indikacija da je nadjen opis polja za unos
int ans_num_p, ans_num_n; //kontrola ukupnog broja tacnih/netacnih odgovora
extern int code_style;
extern int all_answers_wrong; //da li se u radio pitanja ubacuje stavka "Svi prethodno ponudjeni odgovori..."

//pomocne promenljive za rad sa listama
Tchapter* chapter;
Tquestion* question = 0;
Tblock* block = 0;
Tchapters chapters;

//bafer za ispisivanje gresaka (koriste ga makroi za formatiran ispis pomocu yyerror funkcija)
extern char errbuff[1024];

//ovde se cuva trenutno ime fajla koji se parsira
char ccurrent_file[64];

%}

%token _NO_LINE
%token _Q_POSITIVE
%token _Q_ID
%token _Q_NEGATIVE
%token _Q_TEXTBOX
%token _Q_CHECKBOX
%token _Q_END
%token _LN_TEXT
%token _LN_POSITIVE
%token _LN_NEGATIVE
%token _LN_BLOCK
%token _LN_END
%token _LN_TEXTBOX
%token _LN_PICTURE
%token _STRING
%token _Q_LUAID

%%

questions
    :   onequestion { question->has_lua = has_lua; has_lua = 0; }
    |   questions onequestion { question->has_lua = has_lua; has_lua = 0; }
    ;

onequestion
    :   radio_question
    |   text_question
    |   cbox_question
    ;

cbox_question
    :   _Q_ID
        {
            question = insert_question(chapter);
            question->gid = $1;
            question->qid = $1+strlen($1)+1;
            trim_space(question->gid);
            trim_space(question->qid);
            question->id=(char*)malloc0(strlen(question->gid)+strlen(question->qid)+2);
            if (strcmp(question->qid,"")==0) strcpy(question->id,question->gid);
            else sprintf(question->id,"%s*%s",question->gid,question->qid);
            question->line = cline-1;
            make_new_block = 1;
            q_has_tanswer = 0;
            check_qid(chapter);
            question->type = _Q_CHECKBOX;
            ans_num_p = ans_num_n = 0;
        }
        _Q_CHECKBOX optional_luascript cbox_blocks optional_luaid _Q_END
        {
            question->editfield = (int)$3;
            dprintf("QID:%d\n",question->editfield);
            if (ans_num_p == 0) {
                scwarning("No positive answers in question %s",question->id);
            }
            if (ans_num_n == 0) {
                scwarning("No negative answers in question %s",question->id);
            }
            if (ans_num_n + ans_num_p < 2) {
                scwarning("Less than two answers in question %s",question->id);
            }
        }
    ;

cbox_blocks
    :   cbox_block
    |   cbox_blocks _LN_BLOCK cbox_block
    ;

cbox_block
    :   text_lines picture optional_answers
        {
            //ako je poslednja linija bila sa razmakom na pocetku, dodaj tag za zavrsavanje code segmenta
            if (code_style) add_line(&block->text,&block->otext,strdup(""));
            else add_odt_emptyline(&block->otext);
            make_new_block = 1;
//            check_tags(block->text);
            ans_num_p += block->tanswers;
            ans_num_n += block->nanswers;
        }
    ;

radio_question
    :   _Q_ID
        {
            question = insert_question(chapter);
            question->gid = $1;
            question->qid = $1+strlen($1)+1;
            trim_space(question->gid);
            trim_space(question->qid);
            question->id=(char*)malloc0(strlen(question->gid)+strlen(question->qid)+2);
            if (strcmp(question->qid,"")==0) strcpy(question->id,question->gid);
            else sprintf(question->id,"%s*%s",question->gid,question->qid);
            question->line = cline-1;
            make_new_block = 1;
            q_has_tanswer = 0;
            check_qid(chapter);
//            dprintf(" qid:(%s)",$1);
        }
        radio_type
        {
            question->editfield = (int)$3;
            dprintf("QID:%d\n",question->editfield);
            question->type = q_type;
            ans_num_p = ans_num_n = 0;
        }
        optional_luascript radio_blocks optional_luaid _Q_END
        {
            if ((ans_num_p == 0)&&(all_answers_wrong == 0)) {
                scyyerror("No positive answers in question %s",question->id);
            }
            if ((ans_num_n == 0)&&(all_answers_wrong == 0)) {
                scyyerror("No negative answers in question %s",question->id);
            }
            if (ans_num_n + ans_num_p < 2) {
                scwarning("Less than two answers in question %s",question->id);
            }
        }
    ;

radio_type
    :   _Q_POSITIVE
        {
            q_type=_Q_POSITIVE;
        }
    |   _Q_NEGATIVE
        {
            q_type=_Q_NEGATIVE;
        }
    ;

radio_blocks
    :   radio_block
    |   radio_blocks _LN_BLOCK radio_block
    ;

radio_block
    :   text_lines picture optional_answers
        {
            //ako je poslednja linija bila sa razmakom na pocetku, dodaj tag za zavrsavanje code segmenta
            if (code_style) add_line(&block->text,&block->otext,strdup(""));
            else add_odt_emptyline(&block->otext);
            make_new_block = 1;
//            check_tags(block->text);
            ans_num_p += block->tanswers;
            ans_num_n += block->nanswers;
        }
    ;

text_lines
    :   text_line
    |   text_lines text_line
    ;

text_line
    :   _LN_TEXT
        {
            if (make_new_block) {
                block = insert_block(question);
                make_new_block = 0;
                code_style = 0;
            }
            check_tags($1);
            add_line(&block->text,&block->otext,$1);
        }
    ;

picture
    :   /* epty */
    |   _LN_PICTURE
        {
            block->picture = $1;
            trim_space(block->picture);
        }
    ;

optional_answers
    :   /* empty */
    |   answer_lines
    ;

answer_lines
    :   answer_line
    |   answer_lines answer_line
    ;

answer_line
    :   _LN_NEGATIVE
        {
            if (block->nanswers >= MAX_NANSWERS) scyyerror("More than %d negative answers in block, question %s", MAX_NANSWERS, question->id);
            else {
                block->nanswer[block->nanswers] = $1;
                extract_lua_code(&(block->nanswer[block->nanswers]));
                string_replace(&(block->nanswer[block->nanswers]),"\n"," ");
                string_replace(&(block->nanswer[block->nanswers]),"\t"," ");
                while (string_replace(&(block->nanswer[block->nanswers]),"  "," "));
                if (isspace(block->nanswer[block->nanswers][0])) {
                    char* prefix="<c>";
                    char* postfix="</c>";
                    char* newstr;
                    trim_space(block->nanswer[block->nanswers]);
                    newstr=(char*)malloc(strlen(prefix)+strlen(block->nanswer[block->nanswers])+strlen(postfix)+1);
                    strcpy(newstr,prefix);
                    strcat(newstr,block->nanswer[block->nanswers]);
                    strcat(newstr,postfix);
                    free(block->nanswer[block->nanswers]);
                    block->nanswer[block->nanswers] = newstr;
                } else
                    trim_space(block->nanswer[block->nanswers]);
                block->onanswer[block->nanswers] = strdup(block->nanswer[block->nanswers]);
                check_tags(block->nanswer[block->nanswers]);
                fix_odt_tags(&(block->onanswer[block->nanswers]));
                fix_html_tags(&(block->nanswer[block->nanswers]));
                block->nanswers++;
                check_repeating(block,0);
            }
        }
    |   _LN_POSITIVE
        {
            if (q_has_tanswer&&(question->type != _Q_CHECKBOX)) scyyerror("More than 1 positive answer in question %s", question->id);
            else if (block->tanswers >= MAX_TANSWERS) scyyerror("More than %d positive answers in block, question %s", MAX_TANSWERS, question->id);
            else {
                q_has_tanswer = 1;
                block->tanswer[block->tanswers] = $1;
                extract_lua_code(&(block->tanswer[block->tanswers]));
                string_replace(&(block->tanswer[block->tanswers]),"\n"," ");
                string_replace(&(block->tanswer[block->tanswers]),"\t"," ");
                while (string_replace(&(block->tanswer[block->tanswers]),"  "," "));
                if (isspace(block->tanswer[block->tanswers][0])) {
                    char* prefix="<c>";
                    char* postfix="</c>";
                    char* newstr;
                    trim_space(block->tanswer[block->tanswers]);
                    newstr=(char*)malloc(strlen(prefix)+strlen(block->tanswer[block->tanswers])+strlen(postfix)+1);
                    strcpy(newstr,prefix);
                    strcat(newstr,block->tanswer[block->tanswers]);
                    strcat(newstr,postfix);
                    free(block->tanswer[block->tanswers]);
                    block->tanswer[block->tanswers] = newstr;
                } else
                    trim_space(block->tanswer[block->tanswers]);
                block->otanswer[block->tanswers] = strdup(block->tanswer[block->tanswers]);
                check_tags(block->tanswer[block->tanswers]);
                fix_odt_tags(&(block->otanswer[block->tanswers]));
                fix_html_tags(&(block->tanswer[block->tanswers]));
                block->tanswers++;
                check_repeating(block,1);
            }
        }
    ;

text_question
    :   _Q_ID _Q_TEXTBOX
        {
            question = insert_question(chapter);
            question->gid = $1;
            question->qid = $1+strlen($1)+1;
            trim_space(question->gid);
            trim_space(question->qid);
            question->id=(char*)malloc0(strlen(question->gid)+strlen(question->qid)+2);
            if (strcmp(question->qid,"")==0) strcpy(question->id,question->gid);
            else sprintf(question->id,"%s*%s",question->gid,question->qid);
            question->line = cline-1;
            question->type = _Q_TEXTBOX;
            q_has_freetext = 0;
            make_new_block = 1;
            check_qid(chapter);
//            dprintf(" qid:(%s)",$1);
            question->editfield = (int)$2;
            dprintf("QID:%d\n",question->editfield);
        }
        optional_luascript text_blocks optional_luaid _Q_END
        {
            if (!q_has_freetext) scyyerror("No edit field in question %s", question->id);
        }
    ;

text_blocks
    : text_block
    | text_blocks _LN_BLOCK text_block
    ;

text_block
    :   text_lines picture optional_input
        {
            //ako je poslednja linija bila sa razmakom na pocetku, dodaj tag za zavrsavanje code segmenta
            if (code_style) add_line(&block->text,&block->otext,strdup(""));
            else add_odt_emptyline(&block->otext);
            make_new_block = 1;
            check_tags(block->text);
        }
    ;

optional_input
    :   /* empty */
    |   _LN_TEXTBOX
        {
            if (q_has_freetext) scyyerror("More than 1 input field in question %s", question->id);
            else {
                char* c = strstr($1,"\\");
                if (c) {
                    q_has_freetext = 1;
                    block->fttext = (char*)malloc(c-$1+2);
                    strncpy(block->fttext,$1,c-$1);
                    block->fttext[c-$1]=0;
                    block->ftanswer = strdup(c+1);
                    free($1);
                    extract_lua_code(&(block->fttext));
                    extract_lua_code(&(block->ftanswer));
                    check_tags(block->fttext);
                    check_tags(block->ftanswer);
                    fix_html_tags(&(block->fttext));
                    fix_html_tags(&(block->ftanswer));
                    trim_space(block->fttext);
                    trim_space(block->ftanswer);
                }
                else scyyerror("Wrong input field in question %s", question->id);
            }
        }
    ;

optional_luaid
    :   /* empty */
    |   _Q_LUAID
        {
            question->luaid = $1;
            extract_lua_code(&(question->luaid));
            fix_html_tags(&(question->luaid));
        }
    ;

optional_luascript
    :   /* empty */
    |   _Q_LUAID
        {
            question->luascr = $1;
        }
    ;

%%

int cyyerror(char *s) {
  fprintf(stderr, "\033[1;31mERROR in question file %s.txt in line %d: %s\033[0m\n", ccurrent_file, cline, s);
  fprintf(errors, "ERROR in question file %s.txt in line %d: %s\n", ccurrent_file, cline, s);
  error_count++;
  return 0;
}

void cwarning(char *s) {
  fprintf(stderr, "\033[1;34mWARNING in question file %s.txt in line %d: %s\033[0m\n", ccurrent_file, cline, s);
  fprintf(errors, "WARNING in question file %s.txt in line %d: %s\n", ccurrent_file, cline, s);
  warning_count++;
}

void parse_file(char* fname) {
    strcpy(ccurrent_file, fname);
    dprintf("Parsing1:%s\n",fname);
    switch_to_file(fname);
    dprintf("Parsing2:%s\n",fname);
    if (cyyin) {
        chapter = insert_chapter(&chapters);
        chapter->name = strdup(fname);
        dprintf("Parsing3:%s\n",fname);
        cyyparse();
        dprintf("Parsing4:%s\n",fname);
        dprintf("\n");
    }
}

