/*
    This file is part of Testmaker.
    Testmaker (c) 2007,2015 Žarko Živanov

    Testmaker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

%{

//skener za fajlove sa tekstovima pitanja

#include <stdlib.h>
#include <stdio.h>

#include "defs.h"
#include <string.h>
#include "cparser.tab.h"
#include "common.h"

extern YYSTYPE cyylval;
extern int error_count;
int cline;
FILE* last_file = 0;

%}

space               [ \t]
character           [_a-zA-Z0-9\.\-]

%%
"\r"                    {   /* skip*/ }
"/"[tT]{space}*         {   cyylval = (char*)0; return _Q_POSITIVE; }
"/"[nN]{space}*         {   cyylval = (char*)0; return _Q_NEGATIVE; }
"/"[uU]{space}*         {   cyylval = (char*)0; return _Q_TEXTBOX; }
"/"[cC]{space}*         {   cyylval = (char*)0; return _Q_CHECKBOX; }
"/"[tT][eE]{space}*     {   cyylval = (char*)1; return _Q_POSITIVE; }
"/"[nN][eE]{space}*     {   cyylval = (char*)1; return _Q_NEGATIVE; }
"/"[uU][eE]{space}*     {   cyylval = (char*)1; return _Q_TEXTBOX; }
"/"[cC][eE]{space}*     {   cyylval = (char*)1; return _Q_CHECKBOX; }
"="{character}+{space}* {   cyylval = strdup(cyytext+1); return _LN_PICTURE; }
"\\".*                  {   cyylval = strdup(cyytext+1); return _LN_TEXTBOX; }
"."{space}*             {   return _Q_END; }
"/"{space}*             {   return _LN_BLOCK; }
"*".*                   {
                            char* star = strchr(cyytext+1,'*');
                            int l = strlen(cyytext);
                            if (star == NULL) {
                                cyylval = malloc0(l+2);
                                strcpy(cyylval,cyytext+1);
                                strcpy(cyylval+l,"");
                            } else {
                                cyylval = strdup(cyytext+1);
                                *(strchr(cyylval,'*')) = 0;
                            }                            	
                            //l = strlen(cyylval);
                            //printf("txt:(%s) gid:(%s) qid:(%s)\n",cyytext,cyylval,cyylval+l+1);
                            return _Q_ID;
                        }
"-".*                   {   cyylval = strdup(cyytext+1); return _LN_NEGATIVE; }
"+".*                   {   cyylval = strdup(cyytext+1); return _LN_POSITIVE; }
"|".*                   {   cyylval = strdup(cyytext+1); return _Q_LUAID; }
\/\/.*                  {   /* skip comment */ }
{space}*                {   /* skip space */ }
.*                      {   cyylval = strdup(cyytext); return _LN_TEXT; }
"\n"                    {   cline++; }

%%

int cyywrap() {
    return 1;
}

//definisanje fajla koji ce se obradjivati skenerom
void switch_to_file(char* fname) {
    char fnamet[1024];
    cyy_flush_buffer(YY_CURRENT_BUFFER);
    cline = 1;
    sprintf(fnamet,"%s/%s.txt",DIR_QUESTIONS,fname);
    if (last_file) fclose(last_file);
    last_file = cyyin = fopen(fnamet,"r");
    if (!cyyin) {
        dprintf("\n");
        styyerror("Question file %s.txt not found!",fname);
        return;
    }
}

