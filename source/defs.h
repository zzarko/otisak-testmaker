/*
    This file is part of Testmaker.
    Testmaker (c) 2007,2015 Žarko Živanov

    Testmaker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*TODO:
- ubacivanje slika u ODT?
- ne stavljati <c> tag ako je na početku razmak i prva sledeća stvar je <c> tag
*/

#ifndef __DEFS_H__
#define __DEFS_H__

//verzija programa
#define VERSION "3.0"

#define YYSTYPE char*

//#define __DEBUG__

//podrazumevani direktorijumi
#define DIR_QUESTIONS "questions"
#define DIR_TESTS "tests"
#define DIR_OUTPUT "output"
#define DIR_TRANSCODE "transcode"
#define DIR_IMAGES "images"
#define DIR_MATERIALS "materials"
#define EMPTY_MATERIAL "empty"
#define DIR_ODT "odt"

//maksimalan broj grupa u jednom terminu na testu
#define MAX_GROUP 2

//maksimalan broj ispravnih odgovora
#define MAX_TANSWERS 10

//maksimalan broj neispravnih odgovora
#define MAX_NANSWERS 10

//maksimalan broj pitanja u grupi
#define MAX_GQUESTIONS 15

//maksimalan broj karaktera za transcode
#define MAX_TRANSCODE 30

//broj razmaka za tab
#define TAB_SIZE 4

//maksimalna velicina bafera za manipulaciju tekstom
#define MAX_BUFFER 2048

//podrazumevani ulazni format
#define DEFAULT_TRANSCODE "utf8"

//makroi koji omogucuju debug ispis
#ifdef __DEBUG__
extern int DebugTrace;
#define dprintf(args...) if (DebugTrace) fprintf(stderr,args)
#define TraceON DebugTrace=1
#define TraceOFF DebugTrace=0
#else
#define dprintf(args...)
#define TraceON
#define TraceOFF
#endif

//proizvoljan ispis sa yyerror (kao printf)
extern char errbuff[];
#define scyyerror(args...) sprintf(errbuff,args), cyyerror(errbuff)
#define styyerror(args...) sprintf(errbuff,args), tyyerror(errbuff)
#define sprinterror(args...) sprintf(errbuff,args), printerror(errbuff)
#define scwarning(args...) sprintf(errbuff,args), cwarning(errbuff)
#define stwarning(args...) sprintf(errbuff,args), twarning(errbuff)

//konstante za otiskov HTML
#define HTML_SPACES "                "
#define HTML_NL "[br]\n"
#define HTML_NBSP "&amp;nbsp;"
#define HTML_CODE_START "[div class=\\\"Code\\\"]\n"HTML_SPACES
#define HTML_CODE_END "[/div]\n"HTML_SPACES

//konstanta za edit polje za vezbanje
#define XML_EDIT_FIELD \
"        <BLOK>\n" \
"            <TEKST>\n" \
"                [div class=\'Code\']Prostor za vežbanje\n" \
"                [textarea][/textarea]\n" \
"                [/div]\n" \
"            </TEKST>\n" \
"            <SLIKA/>\n" \
"        </BLOK>\n"

//definisanje XML fajla za radio button pitanja
#define XML_RADIO_BEGIN "<?xml version='1.0' encoding='UTF-8'?>\n<QUESTION_RADIO_BUTTON>\n"
#define XML_RADIO_END "</QUESTION_RADIO_BUTTON>"
#define XML_RADIO_QBEGIN "    <PITANJE>\n"
#define XML_RADIO_QEND   "    </PITANJE>\n"
#define XML_RADIO_BBEGIN "        <BLOK>\n"
#define XML_RADIO_BEND   "        </BLOK>\n"
#define XML_RADIO_TBEGIN "            <TEKST>\n"
#define XML_RADIO_TEND   "\n            </TEKST>\n"
#define XML_RADIO_PBEGIN "            <SLIKA>"
#define XML_RADIO_PEND   "</SLIKA>\n"
#define XML_RADIO_ABEGIN "            <ODGOVOR POENI=\"$$$\">"
#define XML_RADIO_AEND   "</ODGOVOR>\n"
#define XML_RADIO_LASTBLOCK1 \
"        <BLOK>\n" \
"            <TEKST>\n"
#define XML_RADIO_REMARK \
"            [b]NAPOMENA:[/b]Pogrešan odgovor nosi negativne poene!\n"
#define XML_RADIO_LASTBLOCK2 \
"            [br]Ako odgovor nije poznat, označiti:\n" \
"            </TEKST>\n" \
"            <SLIKA/>\n" \
"        </BLOK>\n"
#define XML_RADIO_PFIRSTTEXTLINE \
"                Označiti [b]TAČAN[/b] odgovor.[br]\n"
#define XML_RADIO_PLASTBLOCK \
"        <BLOK>\n" \
"            <TEKST>\n" \
"                Ako su svi odgovori netačni, označiti:\n" \
"            </TEKST>\n" \
"            <SLIKA/>\n" \
"            <ODGOVOR POENI=\"$$$\">Svi prethodno ponuđeni odgovori su netačni</ODGOVOR>\n" \
"        </BLOK>\n"
#define XML_RADIO_NFIRSTTEXTLINE \
"                Označiti [b]NETAČAN[/b] odgovor.[br]\n"
#define XML_RADIO_NLASTBLOCK \
"        <BLOK>\n" \
"            <TEKST>\n" \
"                Ako su svi odgovori tačni, označiti:\n" \
"            </TEKST>\n" \
"            <SLIKA/>\n" \
"            <ODGOVOR POENI=\"$$$\">Svi prethodno ponuđeni odgovori su tačni</ODGOVOR>\n" \
"        </BLOK>\n"
#define XML_RADIO_NOANSWER \
"        <BLOK>\n" \
"            <TEKST/>\n" \
"            <SLIKA/>\n" \
"            <ODGOVOR POENI=\"0\">__DeFaUlT__Bez odgovora</ODGOVOR>\n" \
"        </BLOK>\n"

//definisanje XML fajla za checkbox pitanja
#define XML_CHECK_BEGIN "<?xml version='1.0' encoding='UTF-8'?>\n<QUESTION_CHECK_BOX>\n"
#define XML_CHECK_END "</QUESTION_CHECK_BOX>"
#define XML_CHECK_QBEGIN "    <PITANJE>\n"
#define XML_CHECK_QEND   "    </PITANJE>\n"
#define XML_CHECK_BBEGIN "        <BLOK>\n"
#define XML_CHECK_BEND   "        </BLOK>\n"
#define XML_CHECK_TBEGIN "            <TEKST>\n"
#define XML_CHECK_TEND   "\n            </TEKST>\n"
#define XML_CHECK_PBEGIN "            <SLIKA>"
#define XML_CHECK_PEND   "</SLIKA>\n"
#define XML_CHECK_ABEGIN "            <ODGOVOR POENI=\"$$$\">"
#define XML_CHECK_AEND   "</ODGOVOR>\n"
#define XML_CHECK_FIRSTTEXTLINE \
"                Označiti [b]TAČNE[/b] odgovore.[br]\n"
#define XML_CHECK_LASTBLOCK1 \
"        <BLOK>\n" \
"            <TEKST>\n"
#define XML_CHECK_REMARK1 \
"            [b]NAPOMENA:[/b]Jedan pogrešan odgovor nosi negativne poene za celo pitanje\n"
#define XML_CHECK_REMARK2 \
"            [b]NAPOMENA:[/b]Jedan pogrešan odgovor nosi 0 poena za celo pitanje\n"
#define XML_CHECK_LASTBLOCK2 \
"            [br]Ako odgovor nije poznat, ostaviti sva polja prazna.\n" \
"            </TEKST>\n" \
"            <SLIKA/>\n" \
"        </BLOK>\n"
#define XML_CHECK_LASTBLOCK3 \
"            [br]Ako odgovor nije poznat, ostaviti sva polja prazna. (broj tačnih odgovora u pitanju: $$$)\n" \
"            </TEKST>\n" \
"            <SLIKA/>\n" \
"        </BLOK>\n"

//definisanje XML fajla za test
#define XML_TEST_BEGIN "<?xml version='1.0' encoding='UTF-8'?>\n<TEST>\n"
#define XML_TEST_END "</TEST>"
#define XML_TEST_TBEGIN "    <NAZIV>[b]"
#define XML_TEST_TEND   "[/b]</NAZIV>\n"
#define XML_TEST_CBEGIN "    <KURS>"
#define XML_TEST_CEND   "</KURS>\n"
#define XML_TEST_DBEGIN "    <UVOD>[i]"
#define XML_TEST_DEND   "[/i]</UVOD>\n"
#define XML_TEST_MIDDLE \
"    <VAZI_OD>2005-01-01 00:00:00</VAZI_OD>\n" \
"    <VAZI_DO>2100-01-01 00:00:00</VAZI_DO>\n" \
"    <TAB_PESAK>1</TAB_PESAK>\n"
#define XML_TEST_MBEGIN "    <TAB_MATERIJAL>"
#define XML_TEST_MEND "</TAB_MATERIJAL>\n"
#define XML_TEST_NBEGIN "    <UKUPNO_PITANJA>"
#define XML_TEST_NEND   "</UKUPNO_PITANJA>\n"
#define XML_TEST_NGBEGIN "    <NEGATIVNI_OD>"
#define XML_TEST_NGEND   "</NEGATIVNI_OD>\n"
#define XML_TEST_SBEGIN "    <SKALIRANJE>"
#define XML_TEST_SEND   "</SKALIRANJE>\n"
#define XML_TEST_GBEGIN "    <GRUPA>"
#define XML_TEST_GEND   "</GRUPA>\n"
#define XML_TEST_DETBEGIN "    <REZULTAT_DETALJI>"
#define XML_TEST_DETEND   "</REZULTAT_DETALJI>\n"

//definisanje XML fajla za freetext pitanja
#define XML_TEXT_BEGIN "<?xml version='1.0' encoding='UTF-8'?>\n<QUESTION_TABLE_TEXT>\n"
#define XML_TEXT_END   "</QUESTION_TABLE_TEXT>"
#define XML_TEXT_QBEGIN "    <PITANJE NEGATIVNI_POENI=\"$$$\">\n"
#define XML_TEXT_QEND   XML_RADIO_QEND
#define XML_TEXT_BBEGIN XML_RADIO_BBEGIN
#define XML_TEXT_BEND   XML_RADIO_BEND
#define XML_TEXT_TBEGIN XML_RADIO_TBEGIN
#define XML_TEXT_TEND   XML_RADIO_TEND
#define XML_TEXT_PBEGIN XML_RADIO_PBEGIN
#define XML_TEXT_PEND   XML_RADIO_PEND
#define XML_TEXT_HBEGIN \
"            <ZAGLAVLJE MESTO=\"gore\" POZICIJA=\"1\"/>\n" \
"            <ZAGLAVLJE MESTO=\"levo\" POZICIJA=\"1\">"
#define XML_TEXT_HEND   "</ZAGLAVLJE>\n"
#define XML_TEXT_ABEGIN "            <ODGOVOR POZICIJA=\"1\" POENI=\"$$$\">"
#define XML_TEXT_AEND   "</ODGOVOR>\n"
#define XML_TEXT_LASTBLOCK1 \
"        <BLOK>\n" \
"            <TEKST>\n" \
"                Ako tačan odgovor nije poznat, polje za odgovor ostaviti prazno.\n"
#define XML_TEXT_REMARK \
"                [br][b]NAPOMENA:[/b]Neispravan odgovor nosi negativne poene!\n"
#define XML_TEXT_LASTBLOCK2 \
"            </TEKST>\n" \
"            <SLIKA/>\n" \
"        </BLOK>\n"

//definisanje XML fajla za ODT
#define XML_ODT_LSBEGIN "<text:list xml:id=\"list288568817\" text:style-name=\"Numbering_20_1\">"
#define XML_ODT_QBEGIN "<text:list-item>\n"
#define XML_ODT_QEND "<text:p text:style-name=\"P4\"/></text:list-item>\n"
#define XML_ODT_TBEGIN "<text:p text:style-name=\"P4\">"
#define XML_ODT_TEND "</text:p>\n"
#define XML_ODT_ALISTBEGIN "<text:list>\n"
#define XML_ODT_ALISTEND "</text:list>\n"
#define XML_ODT_ABEGIN "<text:list-item><text:p text:style-name=\"P4\">"
#define XML_ODT_AEND "</text:p></text:list-item>\n"
#define XML_ODT_CODE_BEGIN "<text:p text:style-name=\"P4\"/>\n"
#define XML_ODT_CODE_END "<text:p text:style-name=\"P4\"/>\n"
#define XML_ODT_CBEGIN "<text:p text:style-name=\"P3\">"
#define XML_ODT_CEND "</text:p>\n"
#define XML_ODT_TABBEGIN "<text:s text:c=\""
#define XML_ODT_TABEND "\"/>"
#define XML_ODT_ANSWER "<text:p text:style-name=\"P4\">Odgovor:_______________</text:p>\n"
#define XML_ODT_ALL_ANSWERS "Svi prethodno ponuđeni odgovori su netačni"
#define XML_ODT_EMPTYLINE "<text:p text:style-name=\"P4\"/>\n"
#define XML_ODT_CORRECT \
"</text:list>\n" \
"<text:p text:style-name=\"Standard\"/>\n" \
"<text:p text:style-name=\"Standard\"/>\n" \
"<text:p text:style-name=\"P2\">Pitanje - Tačni odgovori (broj poena)</text:p>\n"
#define XML_ODT_CORRECT_BEGIN "<text:p text:style-name=\"Standard\">"
#define XML_ODT_CORRECT_END "</text:p>\n"
#define XML_ODT_END \
"</office:text>\n" \
"</office:body>\n" \
"</office:document-content>\n"
#define XML_ODT_TEXTLINE \
"<text:p text:style-name=\"P4\">$$$</text:p>\n"
#define XML_ODT_REMARK \
"NAPOMENA: Jedan pogrešan odgovor nosi 0/negativne poene za celo pitanje"
#define XML_ODT_CNUMBER \
"<text:p text:style-name=\"P5\">(broj tačnih odgovora u pitanju: $$$) </text:p>\n"

#define LUA_BEGIN "{{"
#define LUA_END "}}"
#define LUA_RBEGIN "{${"
#define LUA_REND "}$}"

#endif

