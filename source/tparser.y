/*
    This file is part of Testmaker.
    Testmaker (c) 2007,2015 Žarko Živanov

    Testmaker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LUA is published under MIT licence, see licence.txt in LUA directory
*/

%{

//#define YYDEBUG 1

//parser za fajl sa opisom testa
//#define _GNU_SOURCE
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include "defs.h"
#include "common.h"

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

extern YYSTYPE cyylval;
extern int cyylex(void);
extern char *cyytext;
extern lua_State* L;

int tyyparse(void);
int tyylex(void);
int tyyerror(char *s);

extern int tline;
extern FILE* tyyin;
extern int error_count;
extern int warning_count;
extern char errbuff[1024];
extern Tusedq* lastusedq;
extern Tusedg* lastusedg;

extern Tchapters chapters;
extern Tchapter* chapter;
extern Tquestion* question;
extern Tblock* block;

//handle-ovi za XML fajlove
FILE* radio;
FILE* freetext;
FILE* test;
FILE* check;
FILE* odt;

//tacni odgovori za ODT
char* odtcorrect;
int odt_qnumber;

//ovde ce se nalaziti ukupan broj pitanja na testu
int q_number;
//broj grupe za otisak (0, 1 ili 2)
char g_number[] = "\0\0";
//grupa (0, A ili B)
char group[]="\0\0";

//ovde se cuva trenutno ime fajla koji se parsira
char tcurrent_file[64];

//naziv testa, zajedno sa grupom
char course[64];

//broj poena jednog testa
int tpoints;

//vrsta konverzije
char* transcode_file = 0;

//podrazumevani materijal
char defaultmaterial[64] = EMPTY_MATERIAL;
//materijal koji se kopira
char dirmaterial[64];

//lista za proveru jedninstvenosti testova
Tusedt* usedt;
Tusedt* lastusedt;

//opcije testa
int scaling = 1;
int negative_from = 1;
int default_positive = 1;
int default_negative = -1;
int print_tanswers = 0;
int default_edit_filed = 0;
int srand_init = 1;
int all_answers_wrong = 1;
int test_points = 0;
int test_details = 1;

%}

%token _NAME
%token _NUMBER
%token _STRING
%token _T_END
%token _MATERIAL
%token _LUAINIT

%token _OPTIONS
%token _SKAL
%token _NEGOD
%token _POZ
%token _NEG
%token _ISPT
%token _EDIT
%token _SRAND
%token _ALLANS
%token _TESTP
%token _DETAILS

%%

test_file
    :   options default_material tests
    ;

default_material
    :   /*empty*/
    |   _MATERIAL
        {
            strcpy(defaultmaterial,$1);
        }
    ;

options
    :   /*empty*/
    |   _OPTIONS _NUMBER _NUMBER _NUMBER _NUMBER _NUMBER
        {
            scaling = atoi($2);
            negative_from = atoi($3);
            default_positive = atoi($4);
            default_negative = atoi($5);
            print_tanswers = atoi($6);
            srand(time(0));
        }
    |   _OPTIONS more_options
    ;

more_options
    :   one_option
    |   more_options one_option
    ;

one_option
    :   _SKAL _NUMBER
        { scaling = atoi($2); }
    |   _NEGOD _NUMBER
        { negative_from = atoi($2); }
    |   _POZ _NUMBER
        { default_positive = atoi($2); }
    |   _NEG _NUMBER
        { default_negative = atoi($2); }
    |   _ISPT _NUMBER
        { print_tanswers = atoi($2); }
    |   _EDIT _NUMBER
        { default_edit_filed = atoi($2); }
    |   _SRAND _NUMBER
        {
            srand_init = atoi($2);
            if (srand_init == 0) srand_init = time(0);
            srand(srand_init);
        }
    |   _ALLANS _NUMBER
        { all_answers_wrong = atoi($2); }
    |   _TESTP _NUMBER
        { test_points = atoi($2); }
    |   _DETAILS _NUMBER
        { test_details = atoi($2); }
    ;

tests
    :   onetest
    |   tests onetest
    ;

onetest
    : testname testgroup
    {
        char g;
        Tusedt* ut;

        //provera grupe
        if (strlen($2) != 1)
            tyyerror("Only one char for the group!");
        g = toupper($2[0]);
        if ((g>='A') && (g<='A'+MAX_GROUP-1)) { group[0] = g; g_number[0] = '0' + g - 'A' + 1; }
        else if ((g>='1') && (g<='1'+MAX_GROUP-1)) { group[0] = g - '1' + 'A'; g_number[0] = g; }
        else if (g=='0') { group[0] = g; g_number[0] = g; }
        else styyerror("Wrong group! (it can be 0,1-%d,a-%c,A-%c)",MAX_GROUP,'a'+MAX_GROUP-1,'A'+MAX_GROUP-1);
        sprintf(course,"%s-%s",$1,group);

        //provera da li je test vec bio
        ut=(Tusedt*)malloc0(sizeof(Tusedt));
        ut->line = tline;
        ut->test = strdup(course);
        if (!usedt) usedt = lastusedt = ut;
        else {
            lastusedt->next = ut;
            lastusedt = ut;
            ut = usedt;
            while (ut!=lastusedt) {
                if (strcmp(ut->test,lastusedt->test)==0) {
                    styyerror("Test %s already defined in line %d",ut->test,ut->line);
                    break;
                }
                ut = ut->next;
            }
        }
        //ako se ne navede, koristi se default material (prazan HTML)
        strcpy(dirmaterial, defaultmaterial);
        //racunanje ukupnog broja poena na testu
        tpoints = 0;
    }
    _STRING _STRING material
        {
            char dir[1024], dir2[1024], index[1024];
            char fname[1024];
            FILE *f;

            //direktorijum u kojeg treba staviti sve izgenerisane fajlove
            sprintf(dir,"%s/%s/%s",DIR_OUTPUT,tcurrent_file,course);
            //kreiranje direktorijuma u kojeg treba staviti sve izgenerisane fajlove (ako ne postoji)
            mkdir(dir,(mode_t)0777);
            //kreiranje images direktorijuma (ako ne postoji) i njegovo brisanje
            sprintf(fname,"%s/images",dir);
            mkdir(fname,(mode_t)0777);
            cleardir(fname);
            //kreiranje material direktorijuma (ako ne postoji) i kopiranje materijala
            sprintf(fname,"%s/material",dir);
            mkdir(fname,(mode_t)0777);
            cleardir(fname);
            sprintf(dir2,"%s/%s",DIR_MATERIALS,dirmaterial);
            copydir(dir2,fname);
            //provera da li postoji index.html u materijalima
            strcpy(index,dir2); strcat(index,"/index.html");
            if (!(f=fopen(index,"r"))) {
                strcpy(index,dir2); strcat(index,"/index.htm");
                if (!(f=fopen(index,"r")))
                    styyerror("Material %s doesn't have an index.html file",dirmaterial);
                else fclose(f);
            }
            else fclose(f);

            //kreiranje XML fajlova i punjenje pocetnim tekstom
            sprintf(fname,"%s/QUESTION_RADIO_BUTTON.xml",dir);
            radio = fopen(fname,"w");
            tfprintf(radio,"%s",XML_RADIO_BEGIN);

            sprintf(fname,"%s/QUESTION_CHECK_BOX.xml",dir);
            check = fopen(fname,"w");
            tfprintf(check,"%s",XML_CHECK_BEGIN);

            sprintf(fname,"%s/QUESTION_TABLE_TEXT.xml",dir);
            freetext = fopen(fname,"w");
            tfprintf(freetext,"%s",XML_TEXT_BEGIN);

            sprintf(fname,"%s/TEST.xml",dir);
            test = fopen(fname,"w");
            tfprintf(test,"%s",XML_TEST_BEGIN);

            sprintf(dir2,"%s",DIR_ODT);
            copyfile("content.xml",dir2,dir);
            sprintf(fname,"%s/content.xml",dir);
            odt = fopen(fname,"a");

            tfprintf(test,"%s",XML_TEST_TBEGIN);
//            if (g_number[0] != '0')
//                tfprintf(test,"%s (%s)",$4,group);
//            else
            tfprintf(test,"%s",$4);
            tfprintf(test,"%s",XML_TEST_TEND);

            tfprintf(test,"%s",XML_TEST_CBEGIN);
            tfprintf(test,"%s",$1);
            tfprintf(test,"%s",XML_TEST_CEND);

            tfprintf(test,"%s",XML_TEST_DBEGIN);
            tfprintf(test,"%s",$5);
            tfprintf(test,"%s",XML_TEST_DEND);

            tfprintf(test,"%s",XML_TEST_MIDDLE);

            q_number = 0;

            write_odt_textline(XML_ODT_REMARK);
            write_odt_textline("");
            ofprintf(odt,"%s",XML_ODT_LSBEGIN);

            odtcorrect = strdup(XML_ODT_CORRECT);
            odt_qnumber = 0;

        }
    questions _T_END
        {
            //zatvaranje XML fajlova i punjenje zavrsnim tekstom
            tfprintf(test,"%s",XML_TEST_MBEGIN);
            tfprintf(test,"%s", strcmp(dirmaterial,EMPTY_MATERIAL) ? "2" : "0");
            tfprintf(test,"%s",XML_TEST_MEND);

            tfprintf(test,"%s",XML_TEST_NBEGIN);
            tfprintf(test,"%d",q_number);
            tfprintf(test,"%s",XML_TEST_NEND);

            tfprintf(test,"%s",XML_TEST_GBEGIN);
            tfprintf(test,"%s",g_number);
            tfprintf(test,"%s",XML_TEST_GEND);

            tfprintf(test,"%s",XML_TEST_NGBEGIN);
            tfprintf(test,"%d",negative_from);
            tfprintf(test,"%s",XML_TEST_NGEND);

            tfprintf(test,"%s",XML_TEST_SBEGIN);
            tfprintf(test,"%d",scaling);
            tfprintf(test,"%s",XML_TEST_SEND);

            tfprintf(test,"%s",XML_TEST_DETBEGIN);
            tfprintf(test,"%d",test_details);
            tfprintf(test,"%s",XML_TEST_DETEND);

            tfprintf(test,"%s",XML_TEST_END);
            fclose(test);

            tfprintf(radio,"%s",XML_RADIO_END);
            fclose(radio);

            tfprintf(check,"%s",XML_CHECK_END);
            fclose(check);

            tfprintf(freetext,"%s",XML_TEXT_END);
            fclose(freetext);

            ofprintf(odt,"%s",odtcorrect);
            ofprintf(odt,"%s",XML_ODT_END);
            fclose(odt);
            free(odtcorrect);

            if (test_points && (tpoints/scaling != test_points))
                styyerror("Test %s has %.1f points, it should be %d", course, 1.0*tpoints/scaling, test_points);
        }
    ;

material
    :   /*empty*/
    |   _MATERIAL
        {
            strcpy(dirmaterial,$1);
        }
    ;

testname
    :   _NAME
    |   _NUMBER
    ;

testgroup
    :   _NAME
    |   _NUMBER
    ;

questions
    :   onequestion
    |   questions onequestion
    ;

onequestion
    :   chapter qid pointp pointn exclude
        {
            char positive[32], negative[32];
            int excl = atoi($5);
            int p = atoi($3);
            int n = atoi($4);

            odt_qnumber++;
            //if (exclude<0) tyyerror("Exclude number less than zero!");
            dprintf("making:%s %s %s %s %s\n",$1,$2,$3,$4,$5);
            if ((p == 0) && (n == 0)) {
                sprintf(positive,"%d",default_positive);
                sprintf(negative,"%d",default_negative);
                p = default_positive;
                n = default_negative;
                //excl = -1;
            }
            else {
                strcpy(positive, $3);
                strcpy(negative, $4);
            }
            if (make_xml_question(stdout,$1,$2,lastusedt->test,excl,positive,negative,tline)) {
                q_number++;
                //lastusedq->test = strdup(course);
                //lastusedq->line = tline;
                //lastusedg->line = tline;
            }
            dprintf("qn:%d\n",q_number);
            tpoints += p;
            clear_lua_results();
        }
    ;

chapter
    :   _NAME
    |   _NUMBER
    ;

qid
    :   _NAME
    |   _NUMBER
    ;

pointp
    :   _NUMBER
    ;

pointn
    :   _NUMBER
    ;

exclude
    :   _NUMBER
    ;

%%

int tyyerror(char *s) {
  fprintf(stderr, "\033[1;31mERROR in test file %s.txt in line %d: %s\033[0m\n", tcurrent_file, tline, s);
  fprintf(errors, "ERROR in test file %s.txt in line %d: %s\n", tcurrent_file, tline, s);
  error_count++;
  return 0;
}

void twarning(char *s) {
  fprintf(stderr, "\033[1;34mWARNING in test file %s.txt in line %d: %s\033[0m\n", tcurrent_file, tline, s);
  fprintf(errors, "WARNING in test file %s.txt in line %d: %s\n", tcurrent_file, tline, s);
  warning_count++;
}

int main_proc(int argc, char *argv[]) {
    char buff[128];
    char ferror[64];
    char fwarning[64];
    char dir[1024];

    TraceON;

    while (1) {
        char c;

        opterr=0;
        c = getopt(argc, argv, "e:");
        if (c == -1) break;
        switch(c) {
            case 'e' :
                {
                    transcode_file = strdup(optarg);
                    break;
                }
            case '?' :
            case ':' :
                {
                    sprinterror("Unknown option %c",optopt);
                    break;
                }
            default :
                {
                    sprinterror("Unknown getopt return code 0x%X",c);
                    break;
                }
        }
    }
    if (!transcode_file) transcode_file = strdup(DEFAULT_TRANSCODE);
    if (optind != argc-1) {
        printf("Testmaker %s by Zarko Zivanov\nUsage: %s [-e <encoding>] <test_name>\n",VERSION,argv[0]);
        return 1;
    }
    init_transcode(transcode_file);
    strcpy(tcurrent_file,argv[optind]);
    sprintf(buff,"%s/%s.txt",DIR_TESTS,argv[optind]);
    tyyin = fopen(buff,"r");
    if (!tyyin) {
        sprinterror("Test file %s.txt not found!",argv[optind]);
        return 1;
    }

    //direktorijum u kojeg treba staviti sve izgenerisane testove
    sprintf(dir,"%s/%s",DIR_OUTPUT,tcurrent_file);
    //kreiranje direktorijuma u kojeg treba staviti sve izgenerisane testove (ako ne postoji)
    mkdir(dir,(mode_t)0777);
    //brisanje eventualnog prethodnog sadrzaja direktorijuma za izgenerisane fajlove
    cleardir(dir);

    sprintf(ferror,"%s/%s/errors.txt",DIR_OUTPUT,tcurrent_file);
    errors = fopen(ferror,"w");
    sprintf(fwarning,"%s/%s/warnings.txt",DIR_OUTPUT,tcurrent_file);
    remove(fwarning);

    /* initialize Lua */
    L = luaL_newstate();
    luaL_openlibs(L);

    chapters.chapter = chapters.lastc = 0;
    tyyparse();
    if (warning_count) printf("\033[1;34m%d warning(s)\033[0m\n",warning_count);
    if (error_count) printf("\033[1;31m%d error(s)\033[0m\n",error_count);
    if (error_count) return 1;
    if (error_count + warning_count == 0) printf("\033[0;32mAll OK\033[0m\n");

    fclose(errors);
    if (!error_count) {
        if (warning_count) rename(ferror,fwarning);
        else remove(ferror);
        return 0;
    }
    else return 1;
}

int main(int argc, char *argv[]) {
    int ret;

    //yydebug=1;

    TraceON;

    ret = main_proc(argc,argv);

    return ret;
}

