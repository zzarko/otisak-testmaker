/*
    This file is part of Testmaker.
    Testmaker (c) 2007,2015 Žarko Živanov

    Testmaker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

%{

//skener za fajl sa opisom testa

#include "defs.h"
#include <string.h>
#include "tparser.tab.h"
#include "common.h"

extern YYSTYPE tyylval;
int tline = 1;

%}

space               [ \t]
character           [_a-zA-Z0-9\.\-\*]
numeric             [0-9]
alpha               [_a-zA-Z0-9]


%%

"\r"                    {   /* skip*/ }
{space}+                {   /* skip*/ }
\/\/.*                  {   /* skip comment */ }
\"[^\"]*\"              {   tyylval = strdup(tyytext+1); tyylval[strlen(tyytext)-2]=0; return _STRING; }
[+-]?{numeric}+         {   tyylval = strdup(tyytext); return _NUMBER; }
{alpha}{character}*     {   tyylval = strdup(tyytext); return _NAME; }
"="{character}+         {   tyylval = strdup(tyytext+1); return _MATERIAL; }
"/"                     {   return _OPTIONS; }
"|"{character}+         {   tyylval = strdup(tyytext+1); return _LUAINIT; }
"."{space}*             {   return _T_END; }
"SKAL="                 {   return _SKAL; }
"NEGOD="                {   return _NEGOD; }
"POZ="                  {   return _POZ; }
"NEG="                  {   return _NEG; }
"ISPT="                 {   return _ISPT; }
"EDIT="                 {   return _EDIT; }
"SRAND="                {   return _SRAND; }
"SVIODG="               {   return _ALLANS; }
"TESTP="                {   return _TESTP; }
"DETALJI="              {   return _DETAILS; }
.                       {   styyerror("Invalid character %s",tyytext); }
"\n"                    {   tline++; }

%%

int tyywrap() {
    return 1;
}
