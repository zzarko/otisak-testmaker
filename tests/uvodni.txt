// SKAL-skaliranje NEGOD-negativni od POZ-podrazumevani pozitivni NEG-podrazumevani negativni
// ISPT-ispis broja tačnih odgovora EDIT-edit polje SRAND=vrednost za srand()
// SVIODG-dodavanje stavke "Svi prethodno navedeni odgovori..." u radio pitanja
// TESTP-provera da li svi testovi imaju zadati broj poena
// DETALJI-da li će se prikazati detalji za pitanja na kraju testa

/ SKAL=1000 NEGOD=3 POZ=5000 NEG=-1000 ISPT=0 EDIT=1 SRAND=0 SVIODG=2 TESTP=0 DETALJI=1

//podrazumevani materijal; ako se linija ne navede, podrazumevani materijal će biti "empty"
//("empty" je specijalan materijal koji podrazumeva da se tab sa materijalom neće prikazivati prilikom testiranja)
=mobilni

UVOD 1
"Uvodni test" "Primer izgleda pitanja na stvarnom testu"
//ovde nije naveden materijal, pa će se koristiti podrazumevani ("mobilni")
//oblast pitanje +poeni -poeni izbacivanje
//ubacivanjem 0 za + i za - poene se označava da će se koristiti podrazumevani broj poena (5000 i -1000)
uvod 1 0 0 1
//negativan broj (ili broj veći od broja negativnih odgovora) za poslednji parametar označava da se neće izbacivati odgovori
uvod 2 3000 -1000 -1
uvod 3 0 0 -1
uvod 4 0 0 -1
//ako se navede i oznaka grupe i oznaka pitanja, biće ubačeno baš to pitanje
//kod checkbox pitanja, broj + poena se ravnomerno deli na sve tačne odgovore u pitanju
//iz tog razloga, ako se ne koristi skaliranje, treba odabrati takav broj poena da bude deljiv sa brojem tačnih odgovora
uvod algebra1*p1 0 0 -1
uvod zezalica*1 0 0 0
.

UVOD b
"Uvodni test" "Primer izgleda pitanja na stvarnom testu"
//za ovu grupu se neće se koristiti podrazumevani materijal ("mobilni"), nego će biti "empty"
=empty
//oblast pitanje +poeni -poeni izbacivanje
uvod 1 0 0 -1
uvod 2 0 0 3
uvod 3 0 0 2
uvod 5 0 0 1
//ako se navede samo naziv grupe, biće ubačeno prvo slobodno pitanje iz te grupe
uvod algebra1 0 0 -1
uvod algebra2 9000 -2000 -1
uvod zezalica 0 0 0
.

